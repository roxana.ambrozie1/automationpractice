import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import Utils

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.baseURL)

WebUI.click(findTestObject('Header/MyStorePage/btnSignIn'))

String randomEmail = Utils.CreateRandomEmail()

WebUI.setText(findTestObject('SignInPage/RegistrationSection/txtEmail'), randomEmail)

WebUI.click(findTestObject('SignInPage/RegistrationSection/btnCreateAnAccount'))

WebUI.check(findTestObject('CreateAccountPage/rdoButtonMrs'))

WebUI.setText(findTestObject('CreateAccountPage/txtCustomerFirstName'), 'Alexandra')

WebUI.setText(findTestObject('CreateAccountPage/txtCustomerLastName'), 'Spanache')

WebUI.setEncryptedText(findTestObject('CreateAccountPage/txtPassword'), 'UXUYxn7drXk=')

WebUI.selectOptionByValue(findTestObject('CreateAccountPage/ddlDays'), '17', false)

WebUI.selectOptionByValue(findTestObject('CreateAccountPage/ddlMonth'), '1', false)

WebUI.selectOptionByValue(findTestObject('CreateAccountPage/ddlYears'), '2000', false)

WebUI.check(findTestObject('CreateAccountPage/cbSignUpNewsletter'))

WebUI.check(findTestObject('CreateAccountPage/cbOptIn'))

WebUI.setText(findTestObject('CreateAccountPage/txtFirstName'), 'Alexandra')

WebUI.setText(findTestObject('CreateAccountPage/txtLastName'), 'Spanache')

WebUI.setText(findTestObject('CreateAccountPage/txtCompany'), 'CSV')

WebUI.setText(findTestObject('CreateAccountPage/txtAddress'), 'str. Basmului, nr. 1')

WebUI.setText(findTestObject('CreateAccountPage/txtAddress2'), 'Ap. 15, et. 1')

WebUI.setText(findTestObject('CreateAccountPage/txtCity'), 'Iasi')

WebUI.selectOptionByValue(findTestObject('CreateAccountPage/ddlState'), '1', false)

WebUI.setText(findTestObject('CreateAccountPage/txtZipPostalCode'), '35005')

WebUI.selectOptionByValue(findTestObject('CreateAccountPage/ddlCountry'), '21', false)

WebUI.setText(findTestObject('CreateAccountPage/txtAdditionalInfo'), 'additional info')

WebUI.setText(findTestObject('CreateAccountPage/txtHomePhone'), '0712345678')

WebUI.setText(findTestObject('CreateAccountPage/txtMobilePhone'), '0787654321')

WebUI.verifyElementClickable(findTestObject('CreateAccountPage/txtAliasAddress'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('CreateAccountPage/btnRegister'))

WebUI.verifyTextPresent('MY ACCOUNT', false)

WebUI.closeBrowser()

