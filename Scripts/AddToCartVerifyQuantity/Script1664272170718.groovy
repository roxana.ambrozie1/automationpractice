import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.baseURL)

WebUI.click(findTestObject('Header/MyStorePage/btnWomen'))

WebUI.click(findTestObject('WomenPage/Products/hlkProductTitle'))

WebUI.setText(findTestObject('ProductPage/txtQuantity'), '1')

WebUI.selectOptionByIndex(findTestObject('ProductPage/ddlSize'), 0)

WebUI.click(findTestObject('ProductPage/btnAddToCart'))

WebUI.click(findTestObject('ProductPage/btnContinueShopping'))

WebUI.setText(findTestObject('ProductPage/txtQuantity'), '2')

WebUI.selectOptionByIndex(findTestObject('ProductPage/ddlSize'), 1)

WebUI.click(findTestObject('ProductPage/btnAddToCart'))

WebUI.click(findTestObject('ProductPage/btnContinueShopping'))

WebUI.verifyElementText(findTestObject('Header/MyStorePage/CartSubmenu/lblCartQuantity'), '3')

WebUI.mouseOver(findTestObject('Header/MyStorePage/btnCart'))

WebUI.click(findTestObject('Header/MyStorePage/CartSubmenu/btnRemoveFromCart'))

WebUI.verifyElementText(findTestObject('Header/MyStorePage/CartSubmenu/lblCartQuantity'), '2')

WebUI.closeBrowser()

