import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import java.util.Random

import internal.GlobalVariable
import javax.crypto.*

public class Utils {
    private static final SecretKey key = KeyGenerator.getInstance('DES').generateKey();
	static String CreateRandomEmail() {
		Random objGenerator = new Random()
		int randomNumber = objGenerator.nextInt(1000)
		println("Random No : " + randomNumber)
		String randomEmail = 'dummy' + randomNumber.toString() + '@mail.com'
		return randomEmail
	}
	static String encryptPassword() {
		def cipher = Cipher.getInstance('DES')
		cipher.init(Cipher.ENCRYPT_MODE, Utils.key)
		def encryptedPassword = cipher.doFinal('dummy'.bytes).encodeBase64().toString()
		println("Encrypted password:" + encryptedPassword)
		return encryptedPassword
	}
	static String decryptPassword(String password) {
		def cipher = Cipher.getInstance('DES')
		cipher.init(Cipher.DECRYPT_MODE, Utils.key)
		def decryptedPassword = new String(cipher.doFinal(Base64.getDecoder().decode(password.bytes)), 'UTF8')
		println("Decrypted password:" + decryptedPassword)
		return decryptedPassword
	}
}
