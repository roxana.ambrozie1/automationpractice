<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btnOtherpaymentMethods</name>
   <tag></tag>
   <elementGuidId>c892d1c1-f3b3-42a9-95f2-076b110df398</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a.button-exclusive.btn.btn-default</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[@class='button-exclusive btn btn-default']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>e201d1bd-5679-454a-8c88-eff3e6a546ec</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>http://automationpractice.com/index.php?controller=order&amp;step=3</value>
      <webElementGuid>b8fe5a9b-990b-4319-a7d1-e75aa2411e7d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>button-exclusive btn btn-default</value>
      <webElementGuid>70e5e946-cd78-41dd-8c06-8aae752e1734</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
				Other payment methods
			</value>
      <webElementGuid>96158cc8-834d-4850-b815-c6d410eae46c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;cart_navigation&quot;)/a[@class=&quot;button-exclusive btn btn-default&quot;]</value>
      <webElementGuid>72320f56-219c-4c02-a1ba-818ae5ea69ba</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//p[@id='cart_navigation']/a</value>
      <webElementGuid>b74acebb-087c-4dc5-bdd9-cdc351d47572</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Dollar'])[1]/following::a[1]</value>
      <webElementGuid>c48db56f-d896-4358-9651-75cefd721564</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='$28.00'])[3]/following::a[1]</value>
      <webElementGuid>ea248bb1-63e4-40c1-b8c3-e42e5afe6483</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='I confirm my order'])[1]/preceding::a[1]</value>
      <webElementGuid>238f4c78-a56a-4021-883c-3609e76c13e2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Newsletter'])[1]/preceding::a[1]</value>
      <webElementGuid>1a941b1a-64f4-40f0-bd48-75dee7a2ee91</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Other payment methods']/parent::*</value>
      <webElementGuid>48eb03fd-304f-4be3-9b24-b40e8d23ce83</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[@href='http://automationpractice.com/index.php?controller=order&amp;step=3']</value>
      <webElementGuid>acee8702-da69-4d8d-834e-09e72abbf96c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form/p/a</value>
      <webElementGuid>4678b990-3b87-4348-b68c-9bd210b33322</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'http://automationpractice.com/index.php?controller=order&amp;step=3' and (text() = '
				Other payment methods
			' or . = '
				Other payment methods
			')]</value>
      <webElementGuid>4eceaf18-29a5-41c7-a21e-cc10010f3c51</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
