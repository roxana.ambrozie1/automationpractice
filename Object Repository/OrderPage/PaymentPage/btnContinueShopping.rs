<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btnContinueShopping</name>
   <tag></tag>
   <elementGuidId>46576cbe-149b-4305-b857-960eb437531a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a.button-exclusive.btn.btn-default</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[@class='button-exclusive btn btn-default']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>dcde499f-1b58-47bb-a517-82d99575c536</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>http://automationpractice.com/index.php?controller=order&amp;step=2</value>
      <webElementGuid>8f11bbe0-1241-431e-85db-aaaab292aa2d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Previous</value>
      <webElementGuid>2b55e8cc-9f54-4ad8-bc77-a6d2495b6580</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>button-exclusive btn btn-default</value>
      <webElementGuid>bf49ad4d-7f2e-422d-87d2-6147dbc29182</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
							
							Continue shopping
						</value>
      <webElementGuid>6c7d2f95-ec11-4a22-9661-b6f7664af1c2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;center_column&quot;)/div[@class=&quot;paiement_block&quot;]/p[@class=&quot;cart_navigation clearfix&quot;]/a[@class=&quot;button-exclusive btn btn-default&quot;]</value>
      <webElementGuid>7208de32-2608-4279-858e-9916ece9e03b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='center_column']/div/p/a</value>
      <webElementGuid>57a10151-c903-4653-b8cb-75f6f5b70b86</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='(order processing will be longer)'])[2]/following::a[1]</value>
      <webElementGuid>4bdf0140-8ded-4106-8ac0-e46e96098d74</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Newsletter'])[1]/preceding::a[1]</value>
      <webElementGuid>8d698682-115c-4631-bbfb-9a742e79e440</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ok'])[1]/preceding::a[1]</value>
      <webElementGuid>5f95efe4-8049-4beb-b3c5-a334eed1cf46</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[@href='http://automationpractice.com/index.php?controller=order&amp;step=2']</value>
      <webElementGuid>b6ae856e-b914-4294-8b8c-74464172b723</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/p/a</value>
      <webElementGuid>011358f3-56bf-43b0-86c0-864ffad27282</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'http://automationpractice.com/index.php?controller=order&amp;step=2' and @title = 'Previous' and (text() = '
							
							Continue shopping
						' or . = '
							
							Continue shopping
						')]</value>
      <webElementGuid>a992a419-a254-4099-9e66-004c1b4222aa</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
