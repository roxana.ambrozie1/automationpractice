<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btnOtherPaymentMethods</name>
   <tag></tag>
   <elementGuidId>71127a46-33ff-4817-943b-668b306f6afe</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a.button-exclusive.btn.btn-default</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[@class='button-exclusive btn btn-default']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>3236f9c1-33f2-4fea-80f1-74a86c4a17b5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>http://automationpractice.com/index.php?controller=order&amp;step=3</value>
      <webElementGuid>15714f01-343e-4e3b-b1e7-844bb2de5a4e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>button-exclusive btn btn-default</value>
      <webElementGuid>f76223e1-6409-4a4f-a20b-924ace9cb4ba</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
				Other payment methods
			</value>
      <webElementGuid>12f903d9-4671-4805-b38e-0fcea6c4fbea</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;cart_navigation&quot;)/a[@class=&quot;button-exclusive btn btn-default&quot;]</value>
      <webElementGuid>7900b9f1-b142-44a6-a223-549f958dfd14</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//p[@id='cart_navigation']/a</value>
      <webElementGuid>3232701e-a0c1-4007-8f7e-c38b31508df8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Dollar'])[1]/following::a[1]</value>
      <webElementGuid>abd37216-d9ac-4da6-a7de-3ddb7c319e10</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='$28.00'])[3]/following::a[1]</value>
      <webElementGuid>16c650ba-73ad-4d34-86d3-2df696745908</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='I confirm my order'])[1]/preceding::a[1]</value>
      <webElementGuid>17d9ec7f-14e4-46cc-95bc-36d0e8de52b8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Newsletter'])[1]/preceding::a[1]</value>
      <webElementGuid>baf02801-0201-4ad3-be23-4131effff015</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Other payment methods']/parent::*</value>
      <webElementGuid>d7d0666c-2a0c-41e8-b08f-4439ffae6e59</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[@href='http://automationpractice.com/index.php?controller=order&amp;step=3']</value>
      <webElementGuid>3cb5f946-c038-448d-b225-f8304c8634a2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form/p/a</value>
      <webElementGuid>a763f35b-6d03-45f5-a263-4b90241de815</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'http://automationpractice.com/index.php?controller=order&amp;step=3' and (text() = '
				Other payment methods
			' or . = '
				Other payment methods
			')]</value>
      <webElementGuid>355b645a-0e78-4228-9909-2be46efc8a40</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
