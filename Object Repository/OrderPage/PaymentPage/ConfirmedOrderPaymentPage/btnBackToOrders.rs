<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btnBackToOrders</name>
   <tag></tag>
   <elementGuidId>b8c32cf0-5050-4cdc-af55-8f9a341bd214</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a[title='Back to orders']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[@class='button-exclusive btn btn-default']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
