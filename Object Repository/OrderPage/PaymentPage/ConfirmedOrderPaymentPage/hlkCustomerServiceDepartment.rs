<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>hlkCustomerServiceDepartment</name>
   <tag></tag>
   <elementGuidId>a1a24517-6171-4c4b-ad2a-8b102a2fcf10</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#center_column > div > a</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'expert customer support team. ')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
