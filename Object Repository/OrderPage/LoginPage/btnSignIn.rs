<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btnSignIn</name>
   <tag></tag>
   <elementGuidId>c161ad14-7398-4456-a14f-9267160c0cc6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#SubmitLogin</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='SubmitLogin']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>8101aa0b-6528-4d37-b1a3-14a7fcd8cf01</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
								
								Sign in
							</value>
      <webElementGuid>91756bc8-5a31-49cf-b367-105ca9a8e117</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;SubmitLogin&quot;)/span[1]</value>
      <webElementGuid>fd4d0dc7-07b4-4ac4-b076-f023e199611a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//button[@id='SubmitLogin']/span</value>
      <webElementGuid>8753729d-49f9-4143-8677-90c683c452a9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Forgot your password?'])[1]/following::span[1]</value>
      <webElementGuid>21e7d854-7a58-4090-b65a-9f5f9884b83e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Password'])[1]/following::span[2]</value>
      <webElementGuid>fa8b2b52-660b-4c6c-b00f-3415acbf8574</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Newsletter'])[1]/preceding::span[1]</value>
      <webElementGuid>d1d4c13f-5d47-4757-af26-edace6ab879e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ok'])[1]/preceding::span[1]</value>
      <webElementGuid>f66b52f3-5791-4ee2-844d-0feb280f6405</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//p[2]/button/span</value>
      <webElementGuid>27a896b4-f851-48e9-b237-2e572579ed85</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = '
								
								Sign in
							' or . = '
								
								Sign in
							')]</value>
      <webElementGuid>2f788a68-1060-4ed5-8ba7-95b9e2d69426</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
