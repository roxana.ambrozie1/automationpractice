<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btnProceedToCheckout</name>
   <tag></tag>
   <elementGuidId>fbcfe091-6816-48e3-8bef-a0f2e6775788</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button[name='processCarrier']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@name='processCarrier']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>d5d30d55-0dbf-45ea-8649-00222f079b30</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
								Proceed to checkout
								
							</value>
      <webElementGuid>a9192435-4983-4fbc-aa66-aca977b5a082</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;form&quot;)/p[@class=&quot;cart_navigation clearfix&quot;]/button[@class=&quot;button btn btn-default standard-checkout button-medium&quot;]/span[1]</value>
      <webElementGuid>89a6b026-05d1-49f3-9479-084131c89187</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='form']/p/button/span</value>
      <webElementGuid>a346c3eb-e5b0-48e7-b4a6-0cf970520ba2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='(Read the Terms of Service)'])[1]/following::span[1]</value>
      <webElementGuid>793ce333-956c-4ef6-9954-3bee3b427acb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Newsletter'])[1]/preceding::span[1]</value>
      <webElementGuid>ded92806-252f-401b-8365-a05df682506c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ok'])[1]/preceding::span[1]</value>
      <webElementGuid>69bd2646-c08a-4752-8d06-2e0d5244703a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//p/button/span</value>
      <webElementGuid>151f0d40-b675-4aa1-baac-b8e1f804c5f1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = '
								Proceed to checkout
								
							' or . = '
								Proceed to checkout
								
							')]</value>
      <webElementGuid>fd2373f4-0f58-45de-9c74-152954bc36b0</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
