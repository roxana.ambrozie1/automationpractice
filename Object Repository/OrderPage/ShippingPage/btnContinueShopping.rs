<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btnContinueShopping</name>
   <tag></tag>
   <elementGuidId>74d30e0c-9be4-4d9f-95ae-942f5a65dc4e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a[title='Previous']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[@title='Previous']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>3cac7359-986a-4d9a-b516-1a05eac28525</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>http://automationpractice.com/index.php?controller=order&amp;step=1&amp;multi-shipping=</value>
      <webElementGuid>abde4fb1-290d-4575-9620-4b033bbc24fb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Previous</value>
      <webElementGuid>0c78da80-baa2-464c-a288-d7ef8eadb2ff</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>button-exclusive btn btn-default</value>
      <webElementGuid>7ded4045-d63d-4c57-8547-8cba181a5e76</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
								
								Continue shopping
							</value>
      <webElementGuid>472e31a1-3c9c-4c83-8512-253b1e7ed276</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;form&quot;)/p[@class=&quot;cart_navigation clearfix&quot;]/a[@class=&quot;button-exclusive btn btn-default&quot;]</value>
      <webElementGuid>1c10c26b-0d7c-418d-86e7-d733b1f12b32</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='form']/p/a</value>
      <webElementGuid>d9399a1a-4b34-4341-b7a7-9dd4bff7731c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='(Read the Terms of Service)'])[1]/following::a[1]</value>
      <webElementGuid>2f236155-947c-4e49-8c97-f1f3c0fa529b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='I agree to the terms of service and will adhere to them unconditionally.'])[1]/following::a[2]</value>
      <webElementGuid>71650083-6bd4-4a77-ae0a-588f0b1328d7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Proceed to checkout'])[2]/preceding::a[1]</value>
      <webElementGuid>deee8f5e-7ce7-4580-88be-f4fef2fce059</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Newsletter'])[1]/preceding::a[1]</value>
      <webElementGuid>e721a6b0-7410-4de5-be1d-a0563b547782</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[@href='http://automationpractice.com/index.php?controller=order&amp;step=1&amp;multi-shipping='])[3]</value>
      <webElementGuid>1520bbf4-adf9-4031-b4a4-59418f493a18</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form/p/a</value>
      <webElementGuid>b317d6f9-c68c-4d3e-9ec7-63025b544b6b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'http://automationpractice.com/index.php?controller=order&amp;step=1&amp;multi-shipping=' and @title = 'Previous' and (text() = '
								
								Continue shopping
							' or . = '
								
								Continue shopping
							')]</value>
      <webElementGuid>a38477a4-2ec5-41eb-ac18-50b226b7ae07</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
