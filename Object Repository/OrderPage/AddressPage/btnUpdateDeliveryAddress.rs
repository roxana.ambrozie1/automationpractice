<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btnUpdateDeliveryAddress</name>
   <tag></tag>
   <elementGuidId>c885ede1-7433-4e6f-8060-a29fcf48385e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//a[@title='Update'])[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#address_delivery > li > a</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
