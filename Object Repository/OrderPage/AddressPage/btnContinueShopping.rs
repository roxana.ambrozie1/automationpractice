<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btnContinueShopping</name>
   <tag></tag>
   <elementGuidId>11e1e041-1d56-4dce-92e7-5b1065d4be2b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a.button-exclusive.btn.btn-default</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[@title='Previous']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>44aafcf8-8261-400c-a400-47a2f2292e4a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>http://automationpractice.com/index.php?controller=order&amp;step=0</value>
      <webElementGuid>95c5b8a5-5b3d-4495-b12b-49525e9fc136</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Previous</value>
      <webElementGuid>84d5aa1b-9588-4566-a3c0-b493acd76a18</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>button-exclusive btn btn-default</value>
      <webElementGuid>230083c3-82d8-4a65-b8f5-e04c1528efd1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
					
					Continue Shopping
				</value>
      <webElementGuid>b855101a-aa1d-458b-9ded-3dfe2aad7288</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;center_column&quot;)/form[1]/p[@class=&quot;cart_navigation clearfix&quot;]/a[@class=&quot;button-exclusive btn btn-default&quot;]</value>
      <webElementGuid>b97ca789-dfbb-4b18-87d2-2612bca9ca09</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='center_column']/form/p/a</value>
      <webElementGuid>bdfedf81-a6f8-4c3a-95d4-4685be4a07c1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='If you would like to add a comment about your order, please write it in the field below.'])[1]/following::a[1]</value>
      <webElementGuid>dc835bad-31f0-4d18-8b54-cae541ab65b7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Add a new address'])[2]/following::a[1]</value>
      <webElementGuid>b86fc1a4-665a-4fe0-9e38-1b90e56fefdf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Proceed to checkout'])[2]/preceding::a[1]</value>
      <webElementGuid>5d0133a7-6a3e-42ff-979b-d1b5539d27c0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Newsletter'])[1]/preceding::a[1]</value>
      <webElementGuid>96b67c27-30e2-40db-9831-b20733ad82d3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Continue Shopping']/parent::*</value>
      <webElementGuid>eec3bf41-5da5-4851-900d-9146cede6d84</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[@href='http://automationpractice.com/index.php?controller=order&amp;step=0']</value>
      <webElementGuid>00b7a0a8-d03c-4505-ab60-446b89738e3c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form/p/a</value>
      <webElementGuid>a4a270ee-3582-4810-89f9-71543361bf13</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'http://automationpractice.com/index.php?controller=order&amp;step=0' and @title = 'Previous' and (text() = '
					
					Continue Shopping
				' or . = '
					
					Continue Shopping
				')]</value>
      <webElementGuid>c19249d9-f891-4aeb-a74d-82559da9741b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
