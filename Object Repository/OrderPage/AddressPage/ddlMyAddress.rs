<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ddlMyAddress</name>
   <tag></tag>
   <elementGuidId>b509b6ab-7b6a-48d0-915f-de49685334cb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#id_address_delivery</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='id_address_delivery']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>c331860a-f125-4feb-93f0-af68e7ced16e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>id_address_delivery</value>
      <webElementGuid>6b43ba3d-279a-42c7-99bf-d254841a4769</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>id_address_delivery</value>
      <webElementGuid>a5cf9d54-c099-4781-8650-ddaf26338a0d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>address_select form-control</value>
      <webElementGuid>cb4f3411-59af-41e7-9549-a29a6d09d1d0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
											
							My address
						
									</value>
      <webElementGuid>a7f4208f-dceb-497e-972a-3bfffc56c370</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;id_address_delivery&quot;)</value>
      <webElementGuid>c15e420d-af51-449f-9908-c7cb54391973</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='id_address_delivery']</value>
      <webElementGuid>39d77c9d-f2bd-4fe4-9d8d-99464e1f99a2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='uniform-id_address_delivery']/select</value>
      <webElementGuid>762e0d0f-a95e-4960-94b1-07d9666e6733</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='My address'])[1]/following::select[1]</value>
      <webElementGuid>b670e648-a7ca-4f67-b8fb-381545aee87d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Choose a delivery address:'])[1]/following::select[1]</value>
      <webElementGuid>da3600a4-df2b-4482-995e-47c803fbacbe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Use the delivery address as the billing address.'])[1]/preceding::select[1]</value>
      <webElementGuid>7543c5ad-0c65-4935-87d5-e068c7cc36c4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Add a new address'])[1]/preceding::select[1]</value>
      <webElementGuid>5e3150f6-28b9-4500-8047-8befef1b70f9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//select</value>
      <webElementGuid>44db8f08-1512-4e6a-9aab-7869aecbb5ef</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'id_address_delivery' and @id = 'id_address_delivery' and (text() = '
											
							My address
						
									' or . = '
											
							My address
						
									')]</value>
      <webElementGuid>16fce873-23f6-4357-a399-9f672a10e206</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
