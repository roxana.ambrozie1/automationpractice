<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btnAddNewAddress</name>
   <tag></tag>
   <elementGuidId>9059335f-4ed6-42ac-8e9c-23ab1081ba73</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>p.address_add.submit > a.button.button-small.btn.btn-default</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//p[@class='address_add submit']//a[@title='Add']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>6631fd18-80a8-4c8f-981c-577389c0d05c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Add a new address</value>
      <webElementGuid>cf943c18-6f64-425f-85e7-7f25061f20d7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;center_column&quot;)/form[1]/div[@class=&quot;addresses clearfix&quot;]/p[@class=&quot;address_add submit&quot;]/a[@class=&quot;button button-small btn btn-default&quot;]/span[1]</value>
      <webElementGuid>968f2fa8-a46a-42be-acb2-59d1d4a6dec3</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='center_column']/form/div/p/a/span</value>
      <webElementGuid>0d083184-5f42-4c75-bbf8-39bbecb46db6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Update'])[2]/following::span[1]</value>
      <webElementGuid>e3303544-6b29-45b1-9477-4400d617580c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='United States'])[2]/following::span[2]</value>
      <webElementGuid>423749d5-d2fe-4c45-badd-014dcbda37d8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='If you would like to add a comment about your order, please write it in the field below.'])[1]/preceding::span[1]</value>
      <webElementGuid>38641a2a-9618-4632-9681-6f4cfeb96d52</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form/div/p/a/span</value>
      <webElementGuid>efd77a11-e34d-4ccd-bfbf-851aa143ba26</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Add a new address' or . = 'Add a new address')]</value>
      <webElementGuid>56798722-edff-460c-b0e9-32fd95f612d5</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
