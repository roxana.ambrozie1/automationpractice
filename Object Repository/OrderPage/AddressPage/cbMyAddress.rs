<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>cbMyAddress</name>
   <tag></tag>
   <elementGuidId>eb26fcba-6616-4bc4-97cd-9aedb56cb1d3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#addressesAreEquals</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='addressesAreEquals']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>cbf9f7be-a508-4e53-8747-91fb12bec2c9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>checkbox</value>
      <webElementGuid>d840c376-d029-4d8a-b702-857d965ce25c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>same</value>
      <webElementGuid>cf3010ce-e2a5-4745-b66a-032b1b527fec</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>addressesAreEquals</value>
      <webElementGuid>759a5744-caa4-427e-aed7-55493abb5faa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>1</value>
      <webElementGuid>345ac6f8-22df-4f18-82a4-fdb43f3b3efa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>checked</name>
      <type>Main</type>
      <value>checked</value>
      <webElementGuid>07fc2d41-7b71-408e-bf02-4cd23ceeec2c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;addressesAreEquals&quot;)</value>
      <webElementGuid>98c2c404-59aa-4a4f-9fa3-d12a08e7a842</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='addressesAreEquals']</value>
      <webElementGuid>84b2a400-3dae-4905-a82b-e23b000b7325</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='uniform-addressesAreEquals']/span/input</value>
      <webElementGuid>dcd26b83-9785-42b9-a942-17b1daad83a2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span/input</value>
      <webElementGuid>7fc6eafc-09b9-4586-99e1-df1cf6661039</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'checkbox' and @name = 'same' and @id = 'addressesAreEquals' and @checked = 'checked']</value>
      <webElementGuid>775576a6-3fd8-4a11-ad26-987fbed3a8e6</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
