<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btnContinueShopping</name>
   <tag></tag>
   <elementGuidId>c480e376-429b-4757-a077-45cb5c301a3c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a[title*=&quot;Continue&quot;]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[@title='Continue shopping']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>50adff51-751e-4dfd-b849-2baf94447e11</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>http://automationpractice.com/index.php</value>
      <webElementGuid>1c8d22e5-b31b-4554-8704-eaf83a81989b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>button-exclusive btn btn-default</value>
      <webElementGuid>ca543484-d723-42d8-890a-8a1f4ff8b66e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Continue shopping</value>
      <webElementGuid>c535660a-61b2-45b8-aca4-1ada8379468c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
			Continue shopping
		</value>
      <webElementGuid>2928274f-8050-432a-b1d5-1cb295b660b2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;center_column&quot;)/p[@class=&quot;cart_navigation clearfix&quot;]/a[@class=&quot;button-exclusive btn btn-default&quot;]</value>
      <webElementGuid>098cde10-7a33-45a4-9f83-f04454819f63</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='center_column']/p[2]/a[2]</value>
      <webElementGuid>71c2426a-81bf-42f4-9caa-5eb7e0a50b66</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Proceed to checkout'])[2]/following::a[1]</value>
      <webElementGuid>68676e4f-e30a-4cc8-9e3c-6d151659b890</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='$26.00'])[6]/following::a[3]</value>
      <webElementGuid>03ca0daf-c9da-4338-9ba1-57e05113dc4b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Newsletter'])[1]/preceding::a[1]</value>
      <webElementGuid>c26a66e5-e2fc-4335-85a3-a56c20beaf54</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ok'])[1]/preceding::a[1]</value>
      <webElementGuid>695164d2-16ff-4d52-bb58-08a2a254f0a2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[@href='http://automationpractice.com/index.php']</value>
      <webElementGuid>cd2b5b35-3151-4b9d-b2f7-67e37f1c2872</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//p[2]/a[2]</value>
      <webElementGuid>ec858821-420d-4e41-a9a7-2d52a273c3cf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'http://automationpractice.com/index.php' and @title = 'Continue shopping' and (text() = '
			Continue shopping
		' or . = '
			Continue shopping
		')]</value>
      <webElementGuid>f8bdb862-25d5-47a9-88fd-fe52d740ad58</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
