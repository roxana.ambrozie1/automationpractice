<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>lbtnDownloadInvoice</name>
   <tag></tag>
   <elementGuidId>f625465e-e59e-4e98-a759-4427185d1960</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.info-order > p > a</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@class='info-order box']//a[contains(text(),'Download')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
