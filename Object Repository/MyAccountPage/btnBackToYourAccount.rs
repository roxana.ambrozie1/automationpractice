<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btnBackToYourAccount</name>
   <tag></tag>
   <elementGuidId>2698997e-ffd0-4278-9756-960864a53071</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//*[@class='footer_links clearfix']//a)[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>.footer_links li:first-child a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>c614cc4d-0122-48e3-a25a-2e0f9fe340ba</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Back to your account</value>
      <webElementGuid>76335319-43c5-47e7-bbef-3313aa818ef4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;center_column&quot;)/ul[@class=&quot;footer_links clearfix&quot;]/li[1]/a[@class=&quot;btn btn-defaul button button-small&quot;]/span[1]</value>
      <webElementGuid>40c97caf-131c-49f5-868b-f808fe61346e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='center_column']/ul/li/a/span</value>
      <webElementGuid>708046bc-b462-417e-8d95-059b494f4a84</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Add a new address'])[1]/following::span[1]</value>
      <webElementGuid>a6aed812-4188-4bf6-8054-b2b8b9035d18</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Delete'])[1]/following::span[2]</value>
      <webElementGuid>93600b4b-f3ac-4be3-b3d3-a57ebf58ef41</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Home'])[1]/preceding::span[1]</value>
      <webElementGuid>d454f12e-001b-4f2e-95f9-36ff2e7eacee</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Newsletter'])[1]/preceding::span[2]</value>
      <webElementGuid>364d3146-935a-4fe5-99d5-8f7c85157c6e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Back to your account']/parent::*</value>
      <webElementGuid>a1019f09-9259-4cbe-a876-953cdafc38e8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/ul/li/a/span</value>
      <webElementGuid>096b5e4a-8a93-450f-8d2e-d3e77075f11c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = ' Back to your account' or . = ' Back to your account')]</value>
      <webElementGuid>f65f8b9f-c20e-48b2-b345-93985f05accb</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
