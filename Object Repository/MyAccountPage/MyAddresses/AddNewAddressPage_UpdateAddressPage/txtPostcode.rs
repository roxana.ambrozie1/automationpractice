<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>txtPostcode</name>
   <tag></tag>
   <elementGuidId>70e6220e-76dd-4775-8c5f-c5e7f7e674f8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#postcode</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='postcode']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>8a425ad7-7635-4b2e-83af-73b185bb2b0d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>is_required validate form-control uniform-input text focus</value>
      <webElementGuid>be7ccaa7-8988-43ef-8026-2ca275e78552</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-validate</name>
      <type>Main</type>
      <value>isPostCode</value>
      <webElementGuid>2dded23d-9802-4c45-b3fa-7e6f62dbb97a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>cec754c5-0374-4ad5-a9ef-1ad63db344a7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>postcode</value>
      <webElementGuid>57cb325a-6258-481a-a5f2-6e824c9c70f5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>postcode</value>
      <webElementGuid>7c442957-3b25-4ae5-926d-c2ab66f609de</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;postcode&quot;)</value>
      <webElementGuid>d8840d5e-13a5-40bd-972f-bb3dc08bc35e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='postcode']</value>
      <webElementGuid>feaa96fe-2024-473b-8dec-5f8ac2c9c8a5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='add_address']/div[8]/input</value>
      <webElementGuid>0bc91f5a-6e3c-45d0-987f-58832f1b5da9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[8]/input</value>
      <webElementGuid>38058ba4-82df-4e84-9833-1a5795665a88</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'text' and @id = 'postcode' and @name = 'postcode']</value>
      <webElementGuid>b11fab59-e9d8-4222-8f4f-cb08dbc38537</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
