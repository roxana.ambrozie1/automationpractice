<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btnAddANewAddress</name>
   <tag></tag>
   <elementGuidId>b98cc171-6c7a-4318-97a8-01b47aa077e4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@title='Add an address']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Add a new address' or . = 'Add a new address')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>[title='Add an address']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>16cbbe50-41bb-4e40-96d9-5a0023e16b75</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Add a new address</value>
      <webElementGuid>8aba4bbb-b871-412a-9447-f77385301fb6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;center_column&quot;)/div[@class=&quot;clearfix main-page-indent&quot;]/a[@class=&quot;btn btn-default button button-medium&quot;]/span[1]</value>
      <webElementGuid>54c2e764-456b-47b0-8d98-da24a367e466</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='center_column']/div[2]/a/span</value>
      <webElementGuid>988fad22-6a35-4b31-9bfb-793683b20ff8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Delete'])[1]/following::span[1]</value>
      <webElementGuid>ba0e4628-d09d-4ccb-b745-14f7a649ee34</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Update'])[1]/following::span[2]</value>
      <webElementGuid>d0dfc46d-4798-4d40-bb02-dbc24ce6d458</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Back to your account'])[1]/preceding::span[1]</value>
      <webElementGuid>d9306638-8722-4037-aa5f-eda332b46180</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Home'])[1]/preceding::span[2]</value>
      <webElementGuid>37486ece-7d21-48ef-8541-b0cf3f824eca</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Add a new address']/parent::*</value>
      <webElementGuid>368ffb4f-4b3a-4b5e-a25e-54dc1c092764</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/a/span</value>
      <webElementGuid>5ee0d7ef-6615-4628-a3e8-036a68325101</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Add a new address' or . = 'Add a new address')]</value>
      <webElementGuid>48372491-a1d9-40e9-bf2c-a7ea180d8192</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
