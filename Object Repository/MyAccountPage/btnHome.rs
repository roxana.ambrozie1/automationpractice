<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btnHome</name>
   <tag></tag>
   <elementGuidId>669e9a68-267b-495d-b06f-30672ba31b36</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//*[@class='footer_links clearfix']//a)[last()]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>.footer_links li:last-child a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>aabb10d9-3338-4253-a240-20f0c1c9a57c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Home</value>
      <webElementGuid>e1950873-ab70-453f-838b-725c35101ab6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;center_column&quot;)/ul[@class=&quot;footer_links clearfix&quot;]/li[1]/a[@class=&quot;btn btn-default button button-small&quot;]/span[1]</value>
      <webElementGuid>376a258f-b477-4098-b0e6-af0a82ea0d24</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='center_column']/ul/li/a/span</value>
      <webElementGuid>6ac99f95-271a-4931-b7d0-c682c167bcd9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='My wishlists'])[1]/following::span[1]</value>
      <webElementGuid>0487d865-d597-49f8-a875-7cef74e9a3e9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='My personal information'])[1]/following::span[2]</value>
      <webElementGuid>b6a7d2c0-9166-4f9f-86ec-835d057c23b3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Newsletter'])[1]/preceding::span[1]</value>
      <webElementGuid>23578d34-9ea6-4085-8140-fb08ea29905f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ok'])[1]/preceding::span[1]</value>
      <webElementGuid>8b102d12-c752-49c4-9791-491006a8437e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Home']/parent::*</value>
      <webElementGuid>2e69659c-6760-4599-9d7f-7432be6a3bb0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/ul/li/a/span</value>
      <webElementGuid>8de6d420-2e71-49f2-85a9-2dcf3403e1d6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = ' Home' or . = ' Home')]</value>
      <webElementGuid>8595e659-82a5-4124-ab08-da30084c9498</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
