<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btnMyWishlist</name>
   <tag></tag>
   <elementGuidId>91d23dee-af3e-4fa3-874e-44b53f072169</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a[title=&quot;My wishlists&quot;]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='center_column']//a[@title='My wishlists']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>daf0a196-2ef6-4c01-aff7-924d04e66e9e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>My wishlists</value>
      <webElementGuid>7eab61e1-120e-46ed-b7f7-ac5aaa82eb2b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;center_column&quot;)/div[@class=&quot;row addresses-lists&quot;]/div[@class=&quot;col-xs-12 col-sm-6 col-lg-4&quot;]/ul[@class=&quot;myaccount-link-list&quot;]/li[@class=&quot;lnk_wishlist&quot;]/a[1]/span[1]</value>
      <webElementGuid>b47c610d-2a1b-4a9f-a552-f6ba2e1e2f58</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='center_column']/div/div[2]/ul/li/a/span</value>
      <webElementGuid>514afcca-a044-4712-aacd-c26be10dffdd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='My personal information'])[1]/following::span[1]</value>
      <webElementGuid>fbe7016b-720d-41c4-89ff-279de0f5b3b2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='My addresses'])[1]/following::span[2]</value>
      <webElementGuid>9d5a658b-10df-440c-91af-eba6e68aaab3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Home'])[1]/preceding::span[1]</value>
      <webElementGuid>14439366-af7b-420d-b033-f1147685e731</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Newsletter'])[1]/preceding::span[2]</value>
      <webElementGuid>c1bb1a8d-9ad9-4888-8323-a71cfb29889a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='My wishlists']/parent::*</value>
      <webElementGuid>785367fa-c36e-466e-a2dc-1bd05e7d3df7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/ul/li/a/span</value>
      <webElementGuid>bb889742-32e1-4f5e-bc9b-e62340dc79e3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'My wishlists' or . = 'My wishlists')]</value>
      <webElementGuid>8cb704ea-3bbd-403a-a83d-1021f04e1372</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
