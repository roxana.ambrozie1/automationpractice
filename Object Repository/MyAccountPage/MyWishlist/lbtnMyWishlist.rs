<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>lbtnMyWishlist</name>
   <tag></tag>
   <elementGuidId>766d6ed0-0892-4a35-ad84-3e42df24ab9c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(), 'My wishlist')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#wishlist_51881 > td:nth-child(1) > a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>eb668975-8c83-4e5d-8a27-4528469db6e3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>javascript:;</value>
      <webElementGuid>c6874242-b95f-44ae-b599-186810189503</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>javascript:WishlistManage('block-order-detail', '51881');</value>
      <webElementGuid>aa9452c4-56ff-487b-8974-4c4c42f50cfa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
										My wishlist
									</value>
      <webElementGuid>fb48af74-fd33-49d7-bdac-55e3e24da071</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;wishlist_51881&quot;)/td[1]/a[1]</value>
      <webElementGuid>ec951e63-ba81-401f-90b7-2ec1723dba9a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[@onclick=&quot;javascript:WishlistManage('block-order-detail', '51881');&quot;]</value>
      <webElementGuid>535ec5b4-1949-4c9e-86e1-d0b3845ec2b8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='wishlist_51881']/td/a</value>
      <webElementGuid>617acb8f-83dd-4eef-8df3-e4f3ee308954</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'My wishlist')]</value>
      <webElementGuid>9228abdb-c124-428e-9491-423594d9a27a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Delete'])[1]/following::a[1]</value>
      <webElementGuid>5d070cda-51da-49c3-a990-7fcd375de4fc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Direct Link'])[1]/following::a[1]</value>
      <webElementGuid>3850218e-61bc-4867-8143-6e038d4d49ab</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='View'])[1]/preceding::a[1]</value>
      <webElementGuid>bc491698-dc4f-4777-9975-3aaa3e07bcbd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='My wishlist']/parent::*</value>
      <webElementGuid>2e51f2cd-184a-47b9-86c0-d65d0e41a253</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'javascript:;')]</value>
      <webElementGuid>91598203-93bc-4448-86fc-f5f2bf19ef36</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td/a</value>
      <webElementGuid>bbaac8c4-edc0-411d-b112-af054a654e70</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'javascript:;' and (text() = '
										My wishlist
									' or . = '
										My wishlist
									')]</value>
      <webElementGuid>43087e9e-adcb-4667-bf33-7db8c8899cef</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
