<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btnRemove</name>
   <tag></tag>
   <elementGuidId>1550b16b-078d-4043-b34e-75a3d26b080a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>[class='icon']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[@class='icon']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>i</value>
      <webElementGuid>315d4ef9-9631-4114-be83-4ed63cdacc32</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>icon-remove</value>
      <webElementGuid>7119d63b-22c8-4eea-9131-fa161f4522d3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;wishlist_51881&quot;)/td[@class=&quot;wishlist_delete&quot;]/a[@class=&quot;icon&quot;]/i[@class=&quot;icon-remove&quot;]</value>
      <webElementGuid>32d86977-302f-459f-8d5e-5a9a8eeb2b48</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='wishlist_51881']/td[6]/a/i</value>
      <webElementGuid>35724ef9-d56c-4299-aa03-cc82c768abe7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td[6]/a/i</value>
      <webElementGuid>13f6ba9b-92cb-4e55-9641-4dac034ec65c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
