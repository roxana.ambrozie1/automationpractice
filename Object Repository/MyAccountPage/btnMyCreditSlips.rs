<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btnMyCreditSlips</name>
   <tag></tag>
   <elementGuidId>9eca10fd-f213-4d40-9215-1b3484536024</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a[title=&quot;Credit slips&quot;]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='center_column']//a[@title='Credit slips']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>93fe3e08-b195-470e-b494-a018a831d47c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>My credit slips</value>
      <webElementGuid>1dcac77d-fe87-45b0-8033-d532f244b6bf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;center_column&quot;)/div[@class=&quot;row addresses-lists&quot;]/div[@class=&quot;col-xs-12 col-sm-6 col-lg-4&quot;]/ul[@class=&quot;myaccount-link-list&quot;]/li[2]/a[1]/span[1]</value>
      <webElementGuid>25d16105-9869-4494-9426-d53e9ec34e90</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='center_column']/div/div/ul/li[2]/a/span</value>
      <webElementGuid>ca6077e6-aa23-4eae-ba90-285c1b020097</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Order history and details'])[1]/following::span[1]</value>
      <webElementGuid>3f658f45-1c5a-45cc-9106-60574cfe5658</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='My account'])[2]/following::span[2]</value>
      <webElementGuid>9d75dd0c-ce0c-48e1-921b-2c3039d890d0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='My addresses'])[1]/preceding::span[1]</value>
      <webElementGuid>530d6ea3-a3d3-4c03-a306-c8ba4ffebbb2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='My personal information'])[1]/preceding::span[2]</value>
      <webElementGuid>ea75fc59-769d-45ee-9aa9-d9100e33ad9a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='My credit slips']/parent::*</value>
      <webElementGuid>3c097159-2c2b-4604-a6fc-f1fdc424b8c0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[2]/a/span</value>
      <webElementGuid>c11f1ddf-809a-4b29-a9be-5e2e7b0e634d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'My credit slips' or . = 'My credit slips')]</value>
      <webElementGuid>5954e01e-72c9-4cfd-883b-ede68e7c8bb0</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
