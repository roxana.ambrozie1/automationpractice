<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>imgStoreLogoPin</name>
   <tag></tag>
   <elementGuidId>7f767864-fb19-4768-98df-695b7c238bce</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div:nth-child(2)> img[src*='logo']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//div/img[contains(@src, 'logo_stores')])[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>img</value>
      <webElementGuid>ad08515a-1bd5-472e-92ed-f1282815ae24</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>src</name>
      <type>Main</type>
      <value>http://maps.gstatic.com/mapfiles/transparent.png</value>
      <webElementGuid>8e55db58-7a4d-4010-90a4-11bd97473479</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>draggable</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>4b11b9c8-2881-447d-b503-df6a6c419a2a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;map&quot;)/div[1]/div[@class=&quot;gm-style&quot;]/div[2]/div[2]/div[1]/div[3]/div[2]/img[1]</value>
      <webElementGuid>f4cc6d91-b9b9-4f7f-a89d-3851bd09dcbb</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='map']/div/div/div[2]/div[2]/div/div[3]/div[2]/img</value>
      <webElementGuid>05bb0875-c239-4ff8-8118-faf21b7d4cb5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:img</name>
      <type>Main</type>
      <value>(//img[contains(@src,'http://maps.gstatic.com/mapfiles/transparent.png')])[2]</value>
      <webElementGuid>454cdfc7-7424-4799-813a-3e8770afc4d9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div[2]/img</value>
      <webElementGuid>7d8d0d33-9b77-497f-8baf-5df3628a844f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//img[@src = 'http://maps.gstatic.com/mapfiles/transparent.png']</value>
      <webElementGuid>5293ea7a-0d76-49f6-83fe-ad9350431a50</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
