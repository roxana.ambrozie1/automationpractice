<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btnFullscreenView</name>
   <tag></tag>
   <elementGuidId>1ddc2dc3-0c1a-4518-8d0a-403dca62946b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button[title='Toggle fullscreen view']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@title='Toggle fullscreen view']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>DIV</value>
      <webElementGuid>ffa6fe55-9b06-414c-9439-a3043026986f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>xpath1663338441397</value>
      <webElementGuid>889d74d6-fd3b-4392-a296-b1db74fb55d0</webElementGuid>
   </webElementProperties>
</WebElementEntity>
