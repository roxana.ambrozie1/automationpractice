<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btnZoomIn</name>
   <tag></tag>
   <elementGuidId>f24ffe9e-1258-4440-ba2b-ee679076c4e2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button[title='Zoom in']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@title='Zoom in']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>ff9782de-109e-418a-867f-56322ceba73e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>draggable</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>cdd76825-f79f-4713-ac48-5af16727a7da</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-label</name>
      <type>Main</type>
      <value>Zoom out</value>
      <webElementGuid>e37c338a-c44e-42c5-b15b-141aa13e04ae</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Zoom out</value>
      <webElementGuid>a45e04a9-6737-485d-8a69-ebb0a1075122</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>9091e2b6-07f9-4d28-9a10-27dce1282bf6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>gm-control-active</value>
      <webElementGuid>c0906b76-2eb9-4b6f-aea9-44ecb1d1be71</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;map&quot;)/div[1]/div[@class=&quot;gm-style&quot;]/div[13]/div[@class=&quot;gmnoprint gm-bundled-control gm-bundled-control-on-bottom&quot;]/div[@class=&quot;gmnoprint&quot;]/div[1]/button[@class=&quot;gm-control-active&quot;]</value>
      <webElementGuid>c2f59043-e349-40ab-9899-5542b588ecc3</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//button[@type='button'])[7]</value>
      <webElementGuid>bbf4d166-0480-4bd2-aade-5279a8296ca3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='map']/div/div/div[13]/div/div[3]/div/button[2]</value>
      <webElementGuid>cf333043-2629-44d2-bbea-41463952df4c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='To navigate, press the arrow keys.'])[1]/following::button[6]</value>
      <webElementGuid>10e2b396-f0ef-41d3-b61f-c57d725224b6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Our store(s)!'])[2]/following::button[7]</value>
      <webElementGuid>0656ebe3-e1f4-4259-bab8-e0e1fa6cb0d8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Keyboard shortcuts'])[1]/preceding::button[1]</value>
      <webElementGuid>92355c90-ff56-4e1f-bd3a-f0e7e1712cba</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Map Data'])[1]/preceding::button[2]</value>
      <webElementGuid>d94aa2e9-cd42-4ce2-9d36-f34fc595247f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/button[2]</value>
      <webElementGuid>060435aa-7d5c-4652-9450-c010eb96ec61</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@title = 'Zoom out' and @type = 'button']</value>
      <webElementGuid>527c185a-e952-4f3e-be04-7975bdb01501</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
