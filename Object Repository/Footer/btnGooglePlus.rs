<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btnGooglePlus</name>
   <tag></tag>
   <elementGuidId>69f937f5-a2fc-408d-96e7-324c16caee97</elementGuidId>
   <imagePath></imagePath>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@class='google-plus']/a</value>
      </entry>
      <entry>
         <key>IMAGE</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>li.google-plus > a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>7146158d-c29e-4c6e-872a-cb001194a297</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>target</name>
      <type>Main</type>
      <value>_blank</value>
      <webElementGuid>287435f1-307d-47e0-9ffb-9204f22b3b79</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>https://plus.google.com/111979135243110831526/posts</value>
      <webElementGuid>09424601-1e98-4824-87b0-520f89b41b2f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        			Google Plus
        		</value>
      <webElementGuid>e1f260c8-c021-4ec8-8aa7-68b657bacd6b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;social_block&quot;)/ul[1]/li[@class=&quot;google-plus&quot;]/a[1]</value>
      <webElementGuid>c2812c76-f548-48e0-b7e9-27028790a50a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='social_block']/ul/li[4]/a</value>
      <webElementGuid>4f9b5738-fe11-440c-a3d1-aa69b7a84dfb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Youtube'])[1]/following::a[1]</value>
      <webElementGuid>0686bc63-f8a9-4b9e-9994-40738d5b7055</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Twitter'])[1]/following::a[2]</value>
      <webElementGuid>e8fdd7de-4398-41d4-89e1-89f986cc8d12</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Follow us'])[1]/preceding::a[1]</value>
      <webElementGuid>e5fd37da-601a-463a-bd35-fec2a03664e6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'https://plus.google.com/111979135243110831526/posts')]</value>
      <webElementGuid>48a68db6-3f6a-4a71-a635-7ad1ca10e5b2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//section/ul/li[4]/a</value>
      <webElementGuid>18baa4ab-7756-41ff-80b8-7c90189562d6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'https://plus.google.com/111979135243110831526/posts' and (text() = '
        			Google Plus
        		' or . = '
        			Google Plus
        		')]</value>
      <webElementGuid>6859f8db-668d-4970-8ca0-039570661dab</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
