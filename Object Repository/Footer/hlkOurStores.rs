<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>hlkOurStores</name>
   <tag></tag>
   <elementGuidId>8a92a07b-16c5-4ad7-a343-779cbaf840b7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@title='Our stores']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a[title=&quot;Our stores&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>a93b4f8f-aa02-4754-bbfa-d0138ab71265</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>http://automationpractice.com/index.php?controller=stores</value>
      <webElementGuid>d89034b4-c79f-4cd1-999c-1549889b2bb3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Our stores</value>
      <webElementGuid>84bce495-c0a5-413f-8c77-75eb1f2b7a3b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
						Our stores
					</value>
      <webElementGuid>3bc26c2d-46de-48c6-8e34-9528af0ed277</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;block_various_links_footer&quot;)/ul[@class=&quot;toggle-footer&quot;]/li[@class=&quot;item&quot;]/a[1]</value>
      <webElementGuid>fa62bda8-78dc-439a-8ded-c69349a9ed79</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='block_various_links_footer']/ul/li[4]/a</value>
      <webElementGuid>11467ce4-5e88-4494-85ee-866eeafabb91</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Our stores')]</value>
      <webElementGuid>a18beec8-3acf-48fc-801f-83425789f8a8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Best sellers'])[1]/following::a[1]</value>
      <webElementGuid>4088d7c3-a1ec-436f-a882-96ac76ed6649</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='New products'])[1]/following::a[2]</value>
      <webElementGuid>86ad8b43-d88e-4e6e-a8f0-ca059ebeede2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Contact us'])[2]/preceding::a[1]</value>
      <webElementGuid>a9eca56f-1613-4a11-9eed-0a3448f7b4f8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Terms and conditions of use'])[1]/preceding::a[2]</value>
      <webElementGuid>81729262-2c3c-4773-9332-c4ce0118bb1d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Our stores']/parent::*</value>
      <webElementGuid>1507fc80-f319-448a-86c7-c91972e163ac</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[@href='http://automationpractice.com/index.php?controller=stores']</value>
      <webElementGuid>0ddc6136-e463-40df-a0b3-7a4646d6e9d4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//section[3]/ul/li[4]/a</value>
      <webElementGuid>bee09768-1813-4bd6-a359-cdb184aae28d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'http://automationpractice.com/index.php?controller=stores' and @title = 'Our stores' and (text() = '
						Our stores
					' or . = '
						Our stores
					')]</value>
      <webElementGuid>781b638f-2b04-4411-9c0f-1e568a9f4f74</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
