<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>hlkBestSellers</name>
   <tag></tag>
   <elementGuidId>03db8f54-e12a-48a2-b3b0-0714df77ab25</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@title='Best sellers']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a[title=&quot;Best sellers&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>5b41e41d-9beb-4bd8-8162-36887c49ba86</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>http://automationpractice.com/index.php?controller=best-sales</value>
      <webElementGuid>f7810250-8c56-4e02-93c2-91f62d81c358</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Best sellers</value>
      <webElementGuid>fbab7bb0-0ded-43df-af87-a91ebe5058cd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
						Best sellers
					</value>
      <webElementGuid>7c63fe60-c315-4d89-95d3-cb6431588dee</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;block_various_links_footer&quot;)/ul[@class=&quot;toggle-footer&quot;]/li[@class=&quot;item&quot;]/a[1]</value>
      <webElementGuid>e5f03046-8b5c-406a-935f-111a14462597</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='block_various_links_footer']/ul/li[3]/a</value>
      <webElementGuid>87de454d-dddc-42b3-a5ae-13e7fb601c06</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Best sellers')]</value>
      <webElementGuid>99c9da97-de80-49ef-b314-385164ede04e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='New products'])[1]/following::a[1]</value>
      <webElementGuid>4a23fbb4-cca6-4513-9655-2f132f5da6db</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Specials'])[1]/following::a[2]</value>
      <webElementGuid>2127ce11-82b3-44e8-bcef-948b1cdaadfe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Our stores'])[1]/preceding::a[1]</value>
      <webElementGuid>ff525c3d-a0e5-4e44-ae62-baf66f39348c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Contact us'])[2]/preceding::a[2]</value>
      <webElementGuid>31680a6b-a63c-40bb-b42f-9ea882e13ee8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Best sellers']/parent::*</value>
      <webElementGuid>0c669c78-54bd-488c-8ec3-89ed59c45862</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[@href='http://automationpractice.com/index.php?controller=best-sales']</value>
      <webElementGuid>d753abdb-45a7-4efd-b04a-82bbdebf9cb7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//section[3]/ul/li[3]/a</value>
      <webElementGuid>14bca840-6496-41d3-b2b3-c8ce03d117b4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'http://automationpractice.com/index.php?controller=best-sales' and @title = 'Best sellers' and (text() = '
						Best sellers
					' or . = '
						Best sellers
					')]</value>
      <webElementGuid>f4aa1e40-b104-47ce-8c4a-1b2d838926dd</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
