<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>hlkAboutUs</name>
   <tag></tag>
   <elementGuidId>03874386-1b76-4be9-9e93-0749b258b77a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@title='About us']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a[title=&quot;About us&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>475efc83-c45d-42f5-bdb9-03e8473bfcca</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>http://automationpractice.com/index.php?id_cms=4&amp;controller=cms</value>
      <webElementGuid>159dca2e-d642-4053-bcc4-68388ef95ed4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>About us</value>
      <webElementGuid>f8b8d650-44af-4832-be20-4031938625d9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
							About us
						</value>
      <webElementGuid>350eb84e-1d61-4c70-8a57-01e6284a74ed</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;block_various_links_footer&quot;)/ul[@class=&quot;toggle-footer&quot;]/li[@class=&quot;item&quot;]/a[1]</value>
      <webElementGuid>abd56dc2-f909-4045-b84c-56f17cca98ce</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='block_various_links_footer']/ul/li[7]/a</value>
      <webElementGuid>25e056ed-7d7c-46bb-8aaa-d8cace8b3df5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'About us')]</value>
      <webElementGuid>20c66948-2b63-47de-a304-dc14fb1d8cc4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Terms and conditions of use'])[1]/following::a[1]</value>
      <webElementGuid>ea0ff3b5-934f-49d8-9a2e-83bec2058090</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Contact us'])[2]/following::a[2]</value>
      <webElementGuid>8ec3dcfe-1318-4c00-b70c-a2f90b089e04</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sitemap'])[1]/preceding::a[1]</value>
      <webElementGuid>3e33980e-7cc4-4447-80e9-cfdda46831fd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='About us']/parent::*</value>
      <webElementGuid>62e6724f-bc9d-487f-bb90-95074e5fe18e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[@href='http://automationpractice.com/index.php?id_cms=4&amp;controller=cms']</value>
      <webElementGuid>deff3508-fbc6-4298-be00-b1a5a2acc541</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[7]/a</value>
      <webElementGuid>7f9a3714-75e6-45f9-8b9a-9d03250ebddf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'http://automationpractice.com/index.php?id_cms=4&amp;controller=cms' and @title = 'About us' and (text() = '
							About us
						' or . = '
							About us
						')]</value>
      <webElementGuid>0abff4d9-b660-4c7a-8d38-8a44c02c89aa</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
