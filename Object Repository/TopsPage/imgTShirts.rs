<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>imgTShirts</name>
   <tag></tag>
   <elementGuidId>ab05c4f2-9bcd-48ba-adbd-d5a0ecab9cea</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@class='img' and @title='T-shirts']/img</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a.img[title='T-shirts']>img</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>img</value>
      <webElementGuid>0311f282-f936-4783-a66c-d97d695f45e1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>replace-2x</value>
      <webElementGuid>fb09a7b3-5fa2-4403-aae7-ed1da89c2de2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>src</name>
      <type>Main</type>
      <value>http://automationpractice.com/img/c/5-medium_default.jpg</value>
      <webElementGuid>30bea54f-e0cb-4e52-a8e2-2181f9f08f59</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>width</name>
      <type>Main</type>
      <value>125</value>
      <webElementGuid>3743015b-5459-4d83-96e1-cdf3e9a60ac4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>height</name>
      <type>Main</type>
      <value>125</value>
      <webElementGuid>95e4f391-476a-4301-aae3-64f801676517</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;subcategories&quot;)/ul[@class=&quot;clearfix&quot;]/li[1]/div[@class=&quot;subcategory-image&quot;]/a[@class=&quot;img&quot;]/img[@class=&quot;replace-2x&quot;]</value>
      <webElementGuid>d8f7eabf-8c90-4c9c-a545-939568b78717</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='subcategories']/ul/li/div/a/img</value>
      <webElementGuid>3ef14b49-be3b-4f1e-af1d-2715c5d66c8e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:img</name>
      <type>Main</type>
      <value>//img[contains(@src,'http://automationpractice.com/img/c/5-medium_default.jpg')]</value>
      <webElementGuid>0b2ed02e-d4a0-4f2c-a722-d989984e201d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li/div/a/img</value>
      <webElementGuid>a30014ea-c2bc-4fa7-8788-6de64969010f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//img[@src = 'http://automationpractice.com/img/c/5-medium_default.jpg']</value>
      <webElementGuid>c328d839-bdbb-4d1c-8f74-221f2cc59ddd</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
