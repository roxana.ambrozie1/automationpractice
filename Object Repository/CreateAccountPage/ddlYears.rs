<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ddlYears</name>
   <tag></tag>
   <elementGuidId>20c66315-df51-4759-b6dd-91406fc115d3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#years</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='years']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
