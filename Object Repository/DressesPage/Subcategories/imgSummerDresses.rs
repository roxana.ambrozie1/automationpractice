<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>imgSummerDresses</name>
   <tag></tag>
   <elementGuidId>6a46d2fd-b6a6-4773-9e13-982d30dbe996</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@class='img' and @title='Summer Dresses']/img</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a.img[title='Summer Dresses']>img</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>img</value>
      <webElementGuid>4a7cc806-547a-465f-a293-26c0a48c2a66</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>replace-2x</value>
      <webElementGuid>69662322-e57f-46ed-9446-78e7f6ba07f7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>src</name>
      <type>Main</type>
      <value>http://automationpractice.com/img/c/11-medium_default.jpg</value>
      <webElementGuid>5a0d2454-0a78-4e5f-a3f7-8ebcb199e581</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>width</name>
      <type>Main</type>
      <value>125</value>
      <webElementGuid>31e3319e-4d5c-449c-a081-1150ee8328bd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>height</name>
      <type>Main</type>
      <value>125</value>
      <webElementGuid>de41c082-b34d-4108-b2b4-161af7844818</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;subcategories&quot;)/ul[@class=&quot;clearfix&quot;]/li[3]/div[@class=&quot;subcategory-image&quot;]/a[@class=&quot;img&quot;]/img[@class=&quot;replace-2x&quot;]</value>
      <webElementGuid>5a9196fd-c7ed-45f7-831c-e61139f77ef8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='subcategories']/ul/li[3]/div/a/img</value>
      <webElementGuid>fc0fc009-7f19-431a-aa49-480a78c556a9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:img</name>
      <type>Main</type>
      <value>//img[contains(@src,'http://automationpractice.com/img/c/11-medium_default.jpg')]</value>
      <webElementGuid>e199ac46-b190-4e09-8774-a3a4dfc27c32</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[3]/div/a/img</value>
      <webElementGuid>fea082b7-7b4d-4c84-a284-2886b7f76eca</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//img[@src = 'http://automationpractice.com/img/c/11-medium_default.jpg']</value>
      <webElementGuid>20b6ca51-3f00-4833-8c80-2d91178ef31a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
