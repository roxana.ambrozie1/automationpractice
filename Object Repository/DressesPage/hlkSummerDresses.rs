<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>hlkSummerDresses</name>
   <tag></tag>
   <elementGuidId>f8854a6b-90c8-4925-a878-9e6599577add</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id='categories_block_left']//a[contains(@title, 'summer')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#categories_block_left a[title*=&quot;summer&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>63af8a4d-98d4-4fb9-922f-b23e4e4d09c4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>http://automationpractice.com/index.php?id_category=11&amp;controller=category</value>
      <webElementGuid>ce04e96a-327b-4ac4-b476-46ca05d35bc6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Short dress, long dress, silk dress, printed dress, you will find the perfect dress for summer.</value>
      <webElementGuid>c7eba589-5b39-48b4-b363-7871c6d817a0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
		Summer Dresses
	</value>
      <webElementGuid>c3059c4a-d12a-4b35-a362-de18ef0942da</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;categories_block_left&quot;)/div[@class=&quot;block_content&quot;]/ul[@class=&quot;tree dynamized&quot;]/li[@class=&quot;last&quot;]/a[1]</value>
      <webElementGuid>79a45ee6-b03d-4862-ad5b-fa235b44c828</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='categories_block_left']/div/ul/li[3]/a</value>
      <webElementGuid>f33e3772-c5f4-4a62-a61d-aebb3940f772</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>(//a[contains(text(),'Summer Dresses')])[3]</value>
      <webElementGuid>d702e500-df40-4588-9cde-7d1e90f4a1c5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Evening Dresses'])[3]/following::a[1]</value>
      <webElementGuid>e30e2535-5336-4c81-9911-680b2f8b07bd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Casual Dresses'])[3]/following::a[2]</value>
      <webElementGuid>fb3e2b0d-ac96-436d-8b5e-608cb137a794</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Categories'])[2]/preceding::a[1]</value>
      <webElementGuid>31d75eaf-3662-45ea-a3e9-08cb42bd8da5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[@href='http://automationpractice.com/index.php?id_category=11&amp;controller=category'])[3]</value>
      <webElementGuid>6e0c9211-6a8e-4bbe-905e-c2c9c88d9d03</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[3]/div/div/div/ul/li[3]/a</value>
      <webElementGuid>8aa36c45-fbae-4812-b0eb-1b5b6d5161c7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'http://automationpractice.com/index.php?id_category=11&amp;controller=category' and @title = 'Short dress, long dress, silk dress, printed dress, you will find the perfect dress for summer.' and (text() = '
		Summer Dresses
	' or . = '
		Summer Dresses
	')]</value>
      <webElementGuid>a5871ffa-a514-4ee6-b5c5-aaf38681f167</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
