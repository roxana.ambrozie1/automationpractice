<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>hlkEveningDresses</name>
   <tag></tag>
   <elementGuidId>54c88de7-8de3-4779-a4d0-197a99f11024</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id='categories_block_left']//a[contains(@title, 'evening')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#categories_block_left a[title*=&quot;evening&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>f1373054-b545-4718-b0ed-d02bfd6ad133</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>http://automationpractice.com/index.php?id_category=10&amp;controller=category</value>
      <webElementGuid>4e81b77d-e521-46ce-89d1-6ad11b271340</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Browse our different dresses to choose the perfect dress for an unforgettable evening!</value>
      <webElementGuid>d1e9311c-55d6-4628-8663-ade1978db0f1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
		Evening Dresses
	</value>
      <webElementGuid>e79f9bb8-8465-4875-ae3b-5226c7aaec76</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;categories_block_left&quot;)/div[@class=&quot;block_content&quot;]/ul[@class=&quot;tree dynamized&quot;]/li[2]/a[1]</value>
      <webElementGuid>b5a2788a-f037-4801-8e15-922ea6b75778</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='categories_block_left']/div/ul/li[2]/a</value>
      <webElementGuid>765dcdd5-3229-42a2-9e0b-cff42f7ed642</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>(//a[contains(text(),'Evening Dresses')])[3]</value>
      <webElementGuid>53fe9619-e05b-422a-8bc9-01c3a87ba6bd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Casual Dresses'])[3]/following::a[1]</value>
      <webElementGuid>691459a8-87a1-4141-8763-c2da5a94c98f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Dresses'])[3]/following::a[2]</value>
      <webElementGuid>ac4517ec-1638-4864-937b-04086e099ad2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Summer Dresses'])[3]/preceding::a[1]</value>
      <webElementGuid>966afc1d-e312-4b5d-97ec-925adea01636</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Categories'])[2]/preceding::a[2]</value>
      <webElementGuid>67bfbd69-beeb-4e6d-9b20-c1f646b9cd3b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[@href='http://automationpractice.com/index.php?id_category=10&amp;controller=category'])[3]</value>
      <webElementGuid>cc7813ce-8f69-401c-abeb-fdf899f4232c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[3]/div/div/div/ul/li[2]/a</value>
      <webElementGuid>1da09554-2c38-4ec0-a6a4-f36586c9fe74</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'http://automationpractice.com/index.php?id_category=10&amp;controller=category' and @title = 'Browse our different dresses to choose the perfect dress for an unforgettable evening!' and (text() = '
		Evening Dresses
	' or . = '
		Evening Dresses
	')]</value>
      <webElementGuid>4c0e1fc6-9dc3-4a40-8f02-33ef61711986</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
