<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>hlkCasualDresses</name>
   <tag></tag>
   <elementGuidId>e3f40e70-4094-4f4e-a64c-5aa8d7e11fe4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id='categories_block_left']//a[contains(@title, 'a dress for every day')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#categories_block_left a[title*=&quot;a dress for every day&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>5281d5b0-102a-4f1d-8ae2-e9df715fa0d5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>http://automationpractice.com/index.php?id_category=9&amp;controller=category</value>
      <webElementGuid>6416220c-4a30-4acf-b886-49d69646bb87</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>You are looking for a dress for every day? Take a look at 
 our selection of dresses to find one that suits you.</value>
      <webElementGuid>8817cc7b-b88e-477d-bfb8-f883e66fca4e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
		Casual Dresses
	</value>
      <webElementGuid>0f42bfcd-8938-4b94-b55b-3ead7f8896d7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;categories_block_left&quot;)/div[@class=&quot;block_content&quot;]/ul[@class=&quot;tree dynamized&quot;]/li[1]/a[1]</value>
      <webElementGuid>0bf7082b-3440-4bf8-a954-4a2eecc23a0b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='categories_block_left']/div/ul/li/a</value>
      <webElementGuid>1c1ab610-4b69-43a9-b07c-e2aba908d633</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>(//a[contains(text(),'Casual Dresses')])[3]</value>
      <webElementGuid>b158ebd4-61a6-4d0b-bae1-4b16f16acd9b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Dresses'])[3]/following::a[1]</value>
      <webElementGuid>ead4bf56-4dd5-4947-bb45-540b7b552ae8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='>'])[2]/following::a[1]</value>
      <webElementGuid>102b6395-30ca-434c-ad16-a1ef81c061d8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Evening Dresses'])[3]/preceding::a[1]</value>
      <webElementGuid>3d6e7113-5f03-4927-8293-86cfa46590dd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Summer Dresses'])[3]/preceding::a[2]</value>
      <webElementGuid>4b842ee2-c4a6-4fe5-aac3-fc842100b9f3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[@href='http://automationpractice.com/index.php?id_category=9&amp;controller=category'])[3]</value>
      <webElementGuid>e70655f5-e06b-456d-8e34-4c39e327fda2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[3]/div/div/div/ul/li/a</value>
      <webElementGuid>483c47ec-4b66-42fe-ad3a-8b2925e45207</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'http://automationpractice.com/index.php?id_category=9&amp;controller=category' and @title = 'You are looking for a dress for every day? Take a look at 
 our selection of dresses to find one that suits you.' and (text() = '
		Casual Dresses
	' or . = '
		Casual Dresses
	')]</value>
      <webElementGuid>3d52d333-a83b-4c5a-9f05-5210ec27574b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
