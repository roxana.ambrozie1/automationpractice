<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>lblCustomerService</name>
   <tag></tag>
   <elementGuidId>3ae8b704-174e-4ca5-b676-465ae319b14a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//h1[@class='page-heading bottom-indent']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>h1.page-heading.bottom-indent</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h1</value>
      <webElementGuid>0d70f698-89e6-4397-ad1d-a2e74cbf277f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>page-heading bottom-indent</value>
      <webElementGuid>e1401989-33e8-4b98-8179-7566012adb87</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    Customer service - Contact us</value>
      <webElementGuid>d62b8c08-5d73-47f1-afbb-2f09b9f07cf2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;center_column&quot;)/h1[@class=&quot;page-heading bottom-indent&quot;]</value>
      <webElementGuid>1741607a-d617-45ed-8c70-fc612f48a76a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='center_column']/h1</value>
      <webElementGuid>6ab327ac-a256-439d-840f-0340f00cb4e4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Contact'])[1]/following::h1[1]</value>
      <webElementGuid>9d25b658-f1fd-4ebb-a000-a471e78cb682</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='>'])[1]/following::h1[1]</value>
      <webElementGuid>c2d85a0a-2fbe-4813-89fa-3ab665c6fa61</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='send a message'])[1]/preceding::h1[1]</value>
      <webElementGuid>e6e008b4-e5cd-477c-9e71-cb50800ab7b2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Subject Heading'])[1]/preceding::h1[1]</value>
      <webElementGuid>3432362e-9ab4-435b-b1bd-7a650f907b6c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Customer service - Contact us']/parent::*</value>
      <webElementGuid>e0ef4654-f189-45b2-b84b-48cbc8ecef76</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h1</value>
      <webElementGuid>8ae75357-24ad-4ea7-96f2-bdf2c53af1f0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h1[(text() = '
    Customer service - Contact us' or . = '
    Customer service - Contact us')]</value>
      <webElementGuid>a96e3ca9-6dc7-47b2-aa4b-e0ec245b93aa</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
