<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>hlkDisplayAllPictures</name>
   <tag></tag>
   <elementGuidId>c83f183d-c30f-41f1-b638-9b567982d0b4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@name='resetImages']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a[name=&quot;resetImages&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>d8ebf18e-ce4c-4a78-8389-9b4f6372dd74</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>http://automationpractice.com/index.php?id_product=5&amp;controller=product</value>
      <webElementGuid>b42171e2-8033-4a38-af82-83cc1701d92f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>resetImages</value>
      <webElementGuid>f80f342e-e5e1-42dc-b67c-9392bacb8c8f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
							
							Display all pictures
						</value>
      <webElementGuid>0e3a96cb-9016-4f95-81f4-9771ad33fa42</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;wrapResetImages&quot;)/a[1]</value>
      <webElementGuid>d1c100c6-b098-4fd4-ada2-a53a15044b97</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[@name='resetImages']</value>
      <webElementGuid>dd3f5405-a9e4-49ab-96c6-0f825d301abf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//span[@id='wrapResetImages']/a</value>
      <webElementGuid>93117ed8-706e-4e6c-88ed-785e9a81d104</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Next'])[1]/following::a[1]</value>
      <webElementGuid>119d4325-b54d-470f-b9a3-2d69f061a857</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Previous'])[1]/following::a[6]</value>
      <webElementGuid>d2e63b0b-698e-4fc9-ac37-de4743325f86</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Printed Summer Dress'])[1]/preceding::a[1]</value>
      <webElementGuid>562229b8-f8f7-4df8-bcb0-a0fddc25a15a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Model'])[1]/preceding::a[1]</value>
      <webElementGuid>5e227d46-31dd-4bf6-86f3-6cb0d3193e28</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Display all pictures']/parent::*</value>
      <webElementGuid>b17a6d2a-24ff-445b-9d53-86aa3165101e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[@href='http://automationpractice.com/index.php?id_product=5&amp;controller=product']</value>
      <webElementGuid>9ac83ca9-b1c9-4568-8e62-9121bf908421</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//p/span/a</value>
      <webElementGuid>a8ebef9d-0c6d-4bb1-a369-db346a69c357</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'http://automationpractice.com/index.php?id_product=5&amp;controller=product' and @name = 'resetImages' and (text() = '
							
							Display all pictures
						' or . = '
							
							Display all pictures
						')]</value>
      <webElementGuid>b7f7a6fb-2df3-4da5-8eed-5b8d7eb3bff0</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
