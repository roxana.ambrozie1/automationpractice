<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btnViewLarger</name>
   <tag></tag>
   <elementGuidId>e719b5ab-b70f-40ce-8fbe-9a8e55a35d3f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//span[@class='span_link no-print']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.span_link.no-print</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>e20dd354-573e-4d36-ab53-4aa591cfa5a2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>span_link no-print</value>
      <webElementGuid>23a80198-7a26-466c-bc17-d4a8ca25fa5f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>View larger</value>
      <webElementGuid>f12c9c0f-5100-4185-87f2-bb4c33edd47e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;view_full_size&quot;)/span[@class=&quot;span_link no-print&quot;]</value>
      <webElementGuid>5e0efb4d-b221-4509-b61d-9383e9c05993</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//span[@id='view_full_size']/span</value>
      <webElementGuid>11ec6b04-cecd-4e6c-9863-b3413807de67</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Reduced price!'])[1]/following::span[2]</value>
      <webElementGuid>f4582030-1f34-4cda-a969-5801151a5c83</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='>'])[2]/following::span[3]</value>
      <webElementGuid>bc23d0af-550f-465d-b05a-70a8bc12e3c4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Previous'])[1]/preceding::span[1]</value>
      <webElementGuid>0c8c5219-b7e5-4fda-a988-9c7289486b32</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Next'])[1]/preceding::span[2]</value>
      <webElementGuid>99fe88a8-12e9-47d3-a14b-8ad3b6eb2983</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='View larger']/parent::*</value>
      <webElementGuid>83a6845b-5a73-4566-a48e-7267c8afb2ef</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span[2]/span</value>
      <webElementGuid>c625732f-f203-4ffc-9f43-eb52e4c384f3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'View larger' or . = 'View larger')]</value>
      <webElementGuid>55523ab0-8a06-4d80-847f-bf0fafda7464</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
