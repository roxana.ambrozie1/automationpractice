<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btnContinueShopping</name>
   <tag></tag>
   <elementGuidId>eb97d625-8104-4d78-859e-423abd0a5920</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>[title='Continue shopping']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@title='Continue shopping']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
