<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btnProceedToCheckout</name>
   <tag></tag>
   <elementGuidId>fa9414f8-9960-4dc0-8811-d49ff0919ec6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a[title='Proceed to checkout']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[@title='Proceed to checkout']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
