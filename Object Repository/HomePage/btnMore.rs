<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btnMore</name>
   <tag></tag>
   <elementGuidId>65f10673-d4f4-436e-8eca-0f8e7cb0ab99</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//div[@class='button-container']//following-sibling::a[@class='button lnk_view btn btn-default'])[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#homefeatured > li.ajax_block_product.col-xs-12.col-sm-4.col-md-3.first-in-line.first-item-of-tablet-line.first-item-of-mobile-line > div > div.right-block > div.button-container > a.button.lnk_view.btn.btn-default</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>896b274f-2eb8-42f3-b4b5-6ef85d4372cf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>More</value>
      <webElementGuid>733c2b8d-59ab-4c32-91ba-ecb98ff5eab1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;homefeatured&quot;)/li[@class=&quot;ajax_block_product col-xs-12 col-sm-4 col-md-3 first-in-line first-item-of-tablet-line first-item-of-mobile-line&quot;]/div[@class=&quot;product-container&quot;]/div[@class=&quot;right-block&quot;]/div[@class=&quot;button-container&quot;]/a[@class=&quot;button lnk_view btn btn-default&quot;]/span[1]</value>
      <webElementGuid>6bf6d8b9-12b1-446f-aa7a-9064ac3f49f9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//ul[@id='homefeatured']/li/div/div[2]/div[2]/a[2]/span</value>
      <webElementGuid>ad6004a4-20a8-41fe-b5ba-a1f20243b722</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Add to cart'])[1]/following::span[1]</value>
      <webElementGuid>ccd4c9c3-f43c-41a4-b185-f0ea2be265b8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='$16.51'])[2]/following::span[2]</value>
      <webElementGuid>224ba6d9-8e6e-4115-b3ca-929b7aee38da</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Quick view'])[2]/preceding::span[3]</value>
      <webElementGuid>23f6ccd0-2bae-422e-8b32-dd9585d300c8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='More']/parent::*</value>
      <webElementGuid>758018dd-c891-4d9a-bd1b-ae33bf4d298c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/a[2]/span</value>
      <webElementGuid>d7e378d7-0bcd-4b94-b531-1295c263238f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'More' or . = 'More')]</value>
      <webElementGuid>757c3732-a7bf-4dec-ad23-807bc8b02bc9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
