<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btnAddToCart</name>
   <tag></tag>
   <elementGuidId>b5bc0528-94f2-4e87-a1cd-bbcaa4cec815</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//div[@class='button-container']//following-sibling::a[@class='button ajax_add_to_cart_button btn btn-default'])[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>ul#homefeatured [data-id-product='1']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>53c4d30e-fdec-4996-bc9b-9c56196d70b9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Add to cart</value>
      <webElementGuid>8cb93181-8ddf-4c52-9b25-d838644128df</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;homefeatured&quot;)/li[@class=&quot;ajax_block_product col-xs-12 col-sm-4 col-md-3 last-item-of-mobile-line&quot;]/div[@class=&quot;product-container&quot;]/div[@class=&quot;right-block&quot;]/div[@class=&quot;button-container&quot;]/a[@class=&quot;button ajax_add_to_cart_button btn btn-default&quot;]/span[1]</value>
      <webElementGuid>b9449e79-6f3d-49e9-bde5-d28993a10692</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//ul[@id='homefeatured']/li[2]/div/div[2]/div[2]/a/span</value>
      <webElementGuid>35ca815b-6bd3-457e-97f0-e7a036004f49</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='$27.00'])[2]/following::span[1]</value>
      <webElementGuid>5f87292b-2d20-46f5-8d1c-5072248ef4ed</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Blouse'])[1]/following::span[2]</value>
      <webElementGuid>ad74f80f-20dd-4143-b12f-0e1e7467d7d0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='More'])[2]/preceding::span[1]</value>
      <webElementGuid>94387d69-9684-4152-b202-b77bc22b4c3d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[2]/div/div[2]/div[2]/a/span</value>
      <webElementGuid>58e9e670-e27e-4c44-b858-4d1f13142419</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Add to cart' or . = 'Add to cart')]</value>
      <webElementGuid>14d19fb6-7133-4525-9e57-4e90d66799e3</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
