<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btnWomen</name>
   <tag></tag>
   <elementGuidId>d578e48b-2312-4cb0-bae7-0f5b9a2d1068</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>ul.sf-menu > li:first-child > a</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='block_top_menu']//child::a[@title='Women']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>74e0f41a-f5b1-4e00-8adb-7bf73b74e07e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>http://automationpractice.com/index.php?id_category=3&amp;controller=category</value>
      <webElementGuid>98f6655d-76f8-43d3-b900-2c2a224545ea</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Women</value>
      <webElementGuid>f0f6f123-b42b-4b9c-9373-9675164f195b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>sf-with-ul</value>
      <webElementGuid>af5095f7-e1f6-4e17-aa9f-590d4288548f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Women</value>
      <webElementGuid>6ace0de9-ea73-47a1-b0fc-3f75f8c6c16a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;block_top_menu&quot;)/ul[@class=&quot;sf-menu clearfix menu-content sf-js-enabled sf-arrows&quot;]/li[@class=&quot;sfHover&quot;]/a[@class=&quot;sf-with-ul&quot;]</value>
      <webElementGuid>2f49d7a0-fe87-42f0-a375-6c4e99211709</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='block_top_menu']/ul/li/a</value>
      <webElementGuid>639904dc-c5d9-4df7-aa08-1e3e64e9b067</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Women')]</value>
      <webElementGuid>043c4493-81e2-40c1-b43d-7d3a67de1823</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Categories'])[1]/following::a[1]</value>
      <webElementGuid>7aab5aac-5dac-4978-897b-dcbf8229898a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Proceed to checkout'])[1]/following::a[1]</value>
      <webElementGuid>9db7ee1f-868d-42a6-82be-a452e3c4aa8f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tops'])[1]/preceding::a[1]</value>
      <webElementGuid>181ec99f-af7e-4ed7-93c6-a5bdb04ba55c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='T-shirts'])[1]/preceding::a[2]</value>
      <webElementGuid>88586b62-a508-4888-8613-96f3eb12364d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Women']/parent::*</value>
      <webElementGuid>9b30b007-b3fc-4d38-ab7f-091d63528956</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[@href='http://automationpractice.com/index.php?id_category=3&amp;controller=category']</value>
      <webElementGuid>62802418-13bc-41f7-80fd-7e02b521ef6d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li/a</value>
      <webElementGuid>60b7e2ce-5a47-43f6-bff5-eec977763d38</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'http://automationpractice.com/index.php?id_category=3&amp;controller=category' and @title = 'Women' and (text() = 'Women' or . = 'Women')]</value>
      <webElementGuid>d9d3cc76-de32-4751-86a3-1121907b7985</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
