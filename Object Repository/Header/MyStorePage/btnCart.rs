<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btnCart</name>
   <tag></tag>
   <elementGuidId>523909c1-5460-470a-8605-18e67bcc7da1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a[title=&quot;View my shopping cart&quot;]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@class='shopping_cart']/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>47949996-4e17-45a9-acaa-f56b5a579fa1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>http://automationpractice.com/index.php?controller=order</value>
      <webElementGuid>eb3fe313-67ee-4a8b-a8da-e67c4263e6dd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>View my shopping cart</value>
      <webElementGuid>c1d12b6f-ccc1-4136-a41e-bb3e6ba18fc7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>rel</name>
      <type>Main</type>
      <value>nofollow</value>
      <webElementGuid>70136f4a-2813-4575-8594-4b388ceb0d65</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
			Cart
			0
			Product
			Products
			
							
			(empty)
					</value>
      <webElementGuid>969f347c-3705-4efb-89ca-358c7a9be04d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;header&quot;)/div[3]/div[@class=&quot;container&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-sm-4 clearfix&quot;]/div[@class=&quot;shopping_cart&quot;]/a[1]</value>
      <webElementGuid>81e670d6-8a75-473f-b7ba-62909ac3df17</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//header[@id='header']/div[3]/div/div/div[3]/div/a</value>
      <webElementGuid>4621e151-c5f4-4f83-bee7-902f3140e526</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Search'])[1]/following::a[1]</value>
      <webElementGuid>393946e2-de2b-4a6b-a861-efdb80711d64</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[@href='http://automationpractice.com/index.php?controller=order']</value>
      <webElementGuid>b5af7bf4-1d61-47a8-ba45-c69aed5acb66</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/a</value>
      <webElementGuid>e5b86e65-625d-4e71-8143-ccab17c0dd89</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'http://automationpractice.com/index.php?controller=order' and @title = 'View my shopping cart' and (text() = '
			Cart
			0
			Product
			Products
			
							
			(empty)
					' or . = '
			Cart
			0
			Product
			Products
			
							
			(empty)
					')]</value>
      <webElementGuid>64fc937d-7ee8-4fe9-824a-4b77ec114c27</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
