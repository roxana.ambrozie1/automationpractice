<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btnEveningDresses</name>
   <tag></tag>
   <elementGuidId>06ab97be-50b3-4545-900e-743f80258d3a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//ul[@class='submenu-container clearfix first-in-line-xs']//ul//a[contains(@title,'Evening Dresses')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#block_top_menu > ul > li:nth-child(1) > ul > li:nth-child(2) > ul > li:nth-child(2) > a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
