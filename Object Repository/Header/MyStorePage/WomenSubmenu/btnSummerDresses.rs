<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btnSummerDresses</name>
   <tag></tag>
   <elementGuidId>206e1e46-1924-471b-a977-ae8f6bee24e5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//ul[@class='submenu-container clearfix first-in-line-xs']//ul//a[contains(@title,'Summer Dresses')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#block_top_menu > ul > li:nth-child(1) > ul > li:nth-child(2) > ul > li:nth-child(3) > a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
