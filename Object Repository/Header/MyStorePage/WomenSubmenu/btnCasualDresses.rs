<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btnCasualDresses</name>
   <tag></tag>
   <elementGuidId>7982916c-a119-41aa-baa9-9c9e8b6f2eec</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//ul[@class='submenu-container clearfix first-in-line-xs']//ul//a[contains(@title,'Casual Dresses')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#block_top_menu > ul > li:nth-child(1) > ul > li:nth-child(2) > ul > li:nth-child(1) > a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
