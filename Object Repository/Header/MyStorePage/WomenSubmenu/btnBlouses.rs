<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btnBlouses</name>
   <tag></tag>
   <elementGuidId>662e13bb-c95f-4301-8db7-8f5f767dff82</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//ul[@class='submenu-container clearfix first-in-line-xs']//ul//a[contains(@title,'Blouses')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#block_top_menu > ul > li:nth-child(1) > ul > li:nth-child(1) > ul > li:nth-child(2) > a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
