<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btnRemoveFromCart</name>
   <tag></tag>
   <elementGuidId>bdd3e574-4457-446d-843a-bfed0b291487</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>dt:first-child .ajax_cart_block_remove_link</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[contains(@class, 'cart_block_remove')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
