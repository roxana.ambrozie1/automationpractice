<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>lblCartQuantity</name>
   <tag></tag>
   <elementGuidId>cf073e69-b7cc-4b90-b5e5-e2bd01284a0c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>.shopping_cart .ajax_cart_quantity</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@class='shopping_cart']//*[contains(@class, 'ajax_cart_quantity')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
