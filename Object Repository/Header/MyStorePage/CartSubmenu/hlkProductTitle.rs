<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>hlkProductTitle</name>
   <tag></tag>
   <elementGuidId>209f6f11-975a-4829-81dd-b41fea8c3d3d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>.first_item .product-name a</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//*[@class='product-name']/a)[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
