<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btnAllSpecials</name>
   <tag></tag>
   <elementGuidId>58d6fa5e-1008-4c75-b1b0-6c2413a32983</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@title='All specials']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a[title='All specials']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>cfb6589f-3a87-4b85-b0fc-a27690299fac</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>All specials</value>
      <webElementGuid>c301c5e3-7283-49f4-9ec4-0a1de40a3dfd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;special_block_right&quot;)/div[@class=&quot;block_content products-block&quot;]/div[1]/a[@class=&quot;btn btn-default button button-small&quot;]/span[1]</value>
      <webElementGuid>1f635604-900a-4c60-8453-895210ba3221</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='special_block_right']/div/div/a/span</value>
      <webElementGuid>ad588fbb-b398-41fa-8d4e-64c05b2fde53</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='$30.51'])[1]/following::span[1]</value>
      <webElementGuid>a8398ef4-0866-4133-9c18-fceb1bc58b2b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='$28.98'])[1]/following::span[3]</value>
      <webElementGuid>b230a0ba-02fe-4c13-b607-67326d933661</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Our stores'])[2]/preceding::span[1]</value>
      <webElementGuid>17d23c95-0f96-4a8a-8d0c-33dc7cf7e11f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Discover our stores'])[1]/preceding::span[1]</value>
      <webElementGuid>81c5dcb0-4f08-460d-8aa9-ce01c448fa19</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='All specials']/parent::*</value>
      <webElementGuid>3e149225-09aa-430a-a264-cc19988cd33d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/a/span</value>
      <webElementGuid>d37a7027-15bf-491c-a4f2-c684e3e4dbea</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'All specials' or . = 'All specials')]</value>
      <webElementGuid>22a20f5c-6997-4c0e-85e6-0f2b08187e5f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
