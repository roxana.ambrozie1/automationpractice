<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btnAddToCart</name>
   <tag></tag>
   <elementGuidId>323a874a-67b6-4a20-b5b2-2da9600b40e4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>li:first-child a[title='Add to cart']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//a[@title='Add to cart'])[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
