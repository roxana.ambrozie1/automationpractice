<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>hlkAddToWishlist</name>
   <tag></tag>
   <elementGuidId>f872e422-b2c6-489b-bc4a-7be0358e3b91</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>.addToWishlist.wishlistProd_1</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//a[contains(@class, 'addToWishlist')])[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>b487ebde-2926-4dec-a26c-6fbafe0154ec</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>addToWishlist wishlistProd_1</value>
      <webElementGuid>f6b698c4-5a14-4695-baea-65446b6f257f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>#</value>
      <webElementGuid>8c3f1f95-ae00-4d69-8a8f-80bd01db766a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>rel</name>
      <type>Main</type>
      <value>1</value>
      <webElementGuid>6df5fc66-1058-410f-a5b4-8e3d9d8fb612</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>WishlistCart('wishlist_block_list', 'add', '1', false, 1); return false;</value>
      <webElementGuid>14fcbe37-3204-49db-bdda-8eec226afb72</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
		Add to Wishlist
	</value>
      <webElementGuid>d6dee13d-07ab-4403-8b81-7863adce6d24</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;center_column&quot;)/ul[@class=&quot;product_list row list&quot;]/li[@class=&quot;ajax_block_product first-in-line first-item-of-tablet-line first-item-of-mobile-line col-xs-12&quot;]/div[@class=&quot;product-container&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;right-block col-xs-4 col-xs-12 col-md-4&quot;]/div[@class=&quot;right-block-content row&quot;]/div[@class=&quot;functional-buttons clearfix col-sm-12&quot;]/div[@class=&quot;wishlist&quot;]/a[@class=&quot;addToWishlist wishlistProd_1&quot;]</value>
      <webElementGuid>0747cb0d-5a97-4855-9379-556bdd08a628</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[@onclick=&quot;WishlistCart('wishlist_block_list', 'add', '1', false, 1); return false;&quot;]</value>
      <webElementGuid>1a7e6da8-3b47-4dd7-b58b-b71d3c876ee8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='center_column']/ul/li/div/div/div[3]/div/div[3]/div/a</value>
      <webElementGuid>5ef66783-0601-4091-be96-d06eaa1963c2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Add to Wishlist')]</value>
      <webElementGuid>299bd779-ba31-4fab-b10a-d7f0b14d53c3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='More'])[1]/following::a[1]</value>
      <webElementGuid>3b58a7a2-b8d8-4042-a5c3-90ccf05eb315</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Add to cart'])[1]/following::a[2]</value>
      <webElementGuid>aa93ed71-6177-4cf5-84b8-f6ece3a7d5d8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Add to Compare'])[1]/preceding::a[1]</value>
      <webElementGuid>eb8a4a48-e56f-4d20-8bea-d6e78257e505</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Quick view'])[2]/preceding::a[4]</value>
      <webElementGuid>c91a02a6-b2ab-4651-896b-c818d7209089</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Add to Wishlist']/parent::*</value>
      <webElementGuid>7c117621-d037-4b1d-8a2c-a1f2a0b915b2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, '#')])[34]</value>
      <webElementGuid>42367937-1154-4a09-a6ae-91f1eda10af5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div[3]/div/a</value>
      <webElementGuid>fed19362-c6c1-42fa-9b7f-856f0e6e4108</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '#' and (text() = '
		Add to Wishlist
	' or . = '
		Add to Wishlist
	')]</value>
      <webElementGuid>029d7a2d-c768-4bf1-b0fb-fdad2627b222</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
