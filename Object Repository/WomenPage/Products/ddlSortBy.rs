<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ddlSortBy</name>
   <tag></tag>
   <elementGuidId>ed800c68-eeb4-4751-8897-918d35594800</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='selectProductSort']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#selectProductSort</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>58bf10cf-9f36-4814-bd49-d8fcfa1b50b2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>selectProductSort</value>
      <webElementGuid>f711d306-e981-4f23-bd82-311fe901fdac</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>selectProductSort form-control</value>
      <webElementGuid>26797f7d-5f30-4a26-815c-773412e8cae3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
			--
							Price: Lowest first
				Price: Highest first
						Product Name: A to Z
			Product Name: Z to A
							In stock
						Reference: Lowest first
			Reference: Highest first
		</value>
      <webElementGuid>676bf067-be7c-46df-8b67-f80ad281644c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;selectProductSort&quot;)</value>
      <webElementGuid>893d05ff-8953-4d6f-b418-0297505db67f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='selectProductSort']</value>
      <webElementGuid>8860af4a-3d59-492a-a943-0dd7a295418c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='uniform-selectProductSort']/select</value>
      <webElementGuid>b80878c8-d8c0-4a6e-a25c-211fa86b2ad3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='--'])[1]/following::select[1]</value>
      <webElementGuid>b191c1db-936e-4d51-a216-fd959913d625</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sort by'])[1]/following::select[1]</value>
      <webElementGuid>61dadf7e-2c03-4e0d-beba-9b5ceb594063</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Showing 1 - 7 of 7 items'])[1]/preceding::select[1]</value>
      <webElementGuid>2088341a-e593-4be2-af58-0119b8bac769</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//select</value>
      <webElementGuid>0b4c2fa5-5282-4b83-a79e-3495a3d17a51</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@id = 'selectProductSort' and (text() = '
			--
							Price: Lowest first
				Price: Highest first
						Product Name: A to Z
			Product Name: Z to A
							In stock
						Reference: Lowest first
			Reference: Highest first
		' or . = '
			--
							Price: Lowest first
				Price: Highest first
						Product Name: A to Z
			Product Name: Z to A
							In stock
						Reference: Lowest first
			Reference: Highest first
		')]</value>
      <webElementGuid>8e64207e-007a-49b4-9975-0a58bc0b1d44</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
