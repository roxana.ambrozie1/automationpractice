<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>hlkProductTitle</name>
   <tag></tag>
   <elementGuidId>8ece3b6e-c48c-4378-9280-771ad993168a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>li:first-child h5[itemprop='name'] a</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//h5[@itemprop='name'])[1]/a</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
