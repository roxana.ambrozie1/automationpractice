<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btnCompare</name>
   <tag></tag>
   <elementGuidId>aac964f5-6104-4d90-838d-194841475114</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[contains(@class, 'top-pagination')]//button</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>.top-pagination-content button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>form</value>
      <webElementGuid>55e51acc-c14f-400d-a13a-22ee9220170f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>method</name>
      <type>Main</type>
      <value>post</value>
      <webElementGuid>9fed29ce-e3c1-418e-9a00-c2b1533e1f2d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>action</name>
      <type>Main</type>
      <value>http://automationpractice.com/index.php?controller=products-comparison</value>
      <webElementGuid>f233e1c5-7133-4b51-8ef9-e04753daa532</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>compare-form</value>
      <webElementGuid>f63e11d2-5944-4577-8704-53ceb15ed867</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
		
			Compare (0)
		
		
		
	</value>
      <webElementGuid>12e2320a-3582-4fe9-b2bd-d2da7af45ff4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;center_column&quot;)/div[@class=&quot;content_sortPagiBar clearfix&quot;]/div[@class=&quot;top-pagination-content clearfix&quot;]/form[@class=&quot;compare-form&quot;]</value>
      <webElementGuid>7b9ad2d5-c897-4037-b966-2029b3b51e87</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//form[@action='http://automationpractice.com/index.php?controller=products-comparison']</value>
      <webElementGuid>cd175666-6500-4bd3-9cfe-54e50bc3e747</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='center_column']/div[3]/div[2]/form</value>
      <webElementGuid>3f2aceb4-1b5f-4c4e-9565-fd64b90c1feb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='--'])[1]/following::form[1]</value>
      <webElementGuid>87647bd0-36e2-4c2c-a03c-843b7ab758f8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sort by'])[1]/following::form[1]</value>
      <webElementGuid>7f9b54be-64ab-4770-8efe-8f31f76ff5db</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Showing 1 - 7 of 7 items'])[1]/preceding::form[1]</value>
      <webElementGuid>4939950f-c9aa-4f73-900f-dfbf946dab15</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div[2]/form</value>
      <webElementGuid>08c9abc3-1121-44b4-a4f5-cfa4ff9b3db0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//form[(text() = '
		
			Compare (0)
		
		
		
	' or . = '
		
			Compare (0)
		
		
		
	')]</value>
      <webElementGuid>0b1bc1f5-2766-44f4-a655-4250321f9b58</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
