<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>cbAvailabilityInStock</name>
   <tag></tag>
   <elementGuidId>651c57a7-d5ce-46fb-b3d4-2815c3229ded</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='layered_quantity_1']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#layered_quantity_1</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>a60b84fc-d569-4fba-80f0-e1c366002110</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>checkbox</value>
      <webElementGuid>7994516d-29b2-4bcd-9988-3e08fc7ef4d4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>checkbox</value>
      <webElementGuid>e6483a3a-fc5d-4909-9949-b1f9f058f75c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>layered_quantity_1</value>
      <webElementGuid>838e5083-bdf4-43cb-a0d8-3873371b59f9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>layered_quantity_1</value>
      <webElementGuid>c1665059-b370-4b19-9971-f65f0b8676f3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>1</value>
      <webElementGuid>ede0fb9c-88d4-4eed-a6a3-b21492f26800</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;layered_quantity_1&quot;)</value>
      <webElementGuid>19d46800-eae0-4f30-b856-2ec474443bb5</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='layered_quantity_1']</value>
      <webElementGuid>72857acf-a846-4a8a-996a-9b9afa5453df</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='uniform-layered_quantity_1']/span/input</value>
      <webElementGuid>c62bca2b-5a3e-4084-a958-eb93552722a0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[7]/ul/li/div/span/input</value>
      <webElementGuid>21cf83ce-73e4-46d5-9110-bdcd588b4136</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'checkbox' and @name = 'layered_quantity_1' and @id = 'layered_quantity_1']</value>
      <webElementGuid>519dc001-2c9f-4f34-b84d-276771dcc681</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
