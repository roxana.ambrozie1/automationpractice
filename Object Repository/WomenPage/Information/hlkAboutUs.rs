<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>hlkAboutUs</name>
   <tag></tag>
   <elementGuidId>1b851f0c-af43-44b9-a2ab-15af5ad12c1a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@class='block_content list-block']//a[@title=&quot;About us&quot;]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>.block_content.list-block a[title=&quot;About us&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>bfacfbd0-554f-4cf9-baf8-7007e36547be</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>http://automationpractice.com/index.php?id_cms=4&amp;controller=cms</value>
      <webElementGuid>059753a9-7968-4963-a152-7dd0886b7341</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>About us</value>
      <webElementGuid>9647dbb1-1bf2-441b-91cd-4d28578ff124</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
									About us
								</value>
      <webElementGuid>de27f2ab-c67b-4dee-8eed-6088cadb0951</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;informations_block_left_1&quot;)/div[@class=&quot;block_content list-block&quot;]/ul[1]/li[4]/a[1]</value>
      <webElementGuid>698e0b63-6cea-4f88-8f78-559f4c67d34f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='informations_block_left_1']/div/ul/li[4]/a</value>
      <webElementGuid>0864c038-783b-48ad-a97a-0bcd63655704</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'About us')]</value>
      <webElementGuid>34e8ecaa-41f2-4c41-b907-808ba84b9d5f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Terms and conditions of use'])[1]/following::a[1]</value>
      <webElementGuid>9a5e2239-5e99-468a-9075-9047c7998f14</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Legal Notice'])[1]/following::a[2]</value>
      <webElementGuid>be1644ba-df72-42d0-9c7f-052dd4a37f75</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Secure payment'])[1]/preceding::a[1]</value>
      <webElementGuid>fbc1a6a9-ac5e-42f9-a37e-c7f122cb20ac</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Our stores'])[1]/preceding::a[2]</value>
      <webElementGuid>e165625e-2265-48a1-bfd0-26e2b6ca0acd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='About us']/parent::*</value>
      <webElementGuid>8909574e-352d-4e66-9236-0b3f51c485ca</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[@href='http://automationpractice.com/index.php?id_cms=4&amp;controller=cms']</value>
      <webElementGuid>a8aead4a-ac0c-4791-8424-dfee3a8fc8cd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[4]/a</value>
      <webElementGuid>f08bc945-cb2f-448c-ae0a-5bd080a98c47</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'http://automationpractice.com/index.php?id_cms=4&amp;controller=cms' and @title = 'About us' and (text() = '
									About us
								' or . = '
									About us
								')]</value>
      <webElementGuid>e5b5e025-437b-450c-94bd-22d79653b855</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
