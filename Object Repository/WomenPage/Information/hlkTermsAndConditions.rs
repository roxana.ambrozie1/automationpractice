<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>hlkTermsAndConditions</name>
   <tag></tag>
   <elementGuidId>88bef050-217a-4ec3-81a9-dba62635240b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@class='block_content list-block']//a[@title='Terms and conditions of use']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>.block_content.list-block a[title=&quot;Terms and conditions of use&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>367d09e0-b825-4bca-8dfa-d6d0a0ca42d0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>http://automationpractice.com/index.php?id_cms=3&amp;controller=cms</value>
      <webElementGuid>2ef22ba2-75a1-41f8-9851-56cfd6d11478</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Terms and conditions of use</value>
      <webElementGuid>538bee6c-5dfc-4db7-9417-bd7bbc54dae2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
									Terms and conditions of use
								</value>
      <webElementGuid>38f3f137-e0c0-4c7b-a0bf-08f66d368161</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;informations_block_left_1&quot;)/div[@class=&quot;block_content list-block&quot;]/ul[1]/li[3]/a[1]</value>
      <webElementGuid>06cfd814-a46c-48a1-898d-cbd1ffcd855d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='informations_block_left_1']/div/ul/li[3]/a</value>
      <webElementGuid>a1136128-715d-4121-8266-435830a9fbcd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Terms and conditions of use')]</value>
      <webElementGuid>5152cbfe-ed14-4add-972d-88e5bedf8f14</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Legal Notice'])[1]/following::a[1]</value>
      <webElementGuid>a75cc0e1-9105-4101-b8ed-67d7a619641f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Delivery'])[1]/following::a[2]</value>
      <webElementGuid>caafee4d-e21f-4bed-8a05-8f513599d954</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='About us'])[1]/preceding::a[1]</value>
      <webElementGuid>7d3c60d4-0a13-4479-9703-949a7a4b0459</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Secure payment'])[1]/preceding::a[2]</value>
      <webElementGuid>4fc5f838-180a-433c-9f33-c2e35c0aed09</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Terms and conditions of use']/parent::*</value>
      <webElementGuid>fe8d528e-c642-495e-bead-0e5d1d004932</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[@href='http://automationpractice.com/index.php?id_cms=3&amp;controller=cms']</value>
      <webElementGuid>9ecfa631-46d7-412b-9028-25054bae3cd5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//section/div/ul/li[3]/a</value>
      <webElementGuid>ca1106ad-739c-4ba8-9fc8-c738268a8b5a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'http://automationpractice.com/index.php?id_cms=3&amp;controller=cms' and @title = 'Terms and conditions of use' and (text() = '
									Terms and conditions of use
								' or . = '
									Terms and conditions of use
								')]</value>
      <webElementGuid>0255903d-b608-409b-8e92-015b6d831a60</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
