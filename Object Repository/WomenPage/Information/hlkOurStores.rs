<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>hlkOurStores</name>
   <tag></tag>
   <elementGuidId>2eea09db-4ef0-48c2-b6cd-20bd42ade35f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@class='block_content list-block']//a[@title=&quot;Our stores&quot;]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>.block_content.list-block a[title=&quot;Our stores&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>b39d2c03-90e1-49da-b36e-632d25641b78</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>http://automationpractice.com/index.php?controller=stores</value>
      <webElementGuid>0fecf128-6cb0-4dc4-bb78-59c3cb303dc5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Our stores</value>
      <webElementGuid>67bfe48e-af21-48c5-8ca4-9246159eb7cf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
								Our stores
							</value>
      <webElementGuid>3dca7cdf-9449-482a-be9d-762fe1dce6b6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;informations_block_left_1&quot;)/div[@class=&quot;block_content list-block&quot;]/ul[1]/li[6]/a[1]</value>
      <webElementGuid>4cf4224f-2505-4bc6-b996-2dce73401119</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='informations_block_left_1']/div/ul/li[6]/a</value>
      <webElementGuid>98100a81-54e7-4210-a14b-8e16ef563fef</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Our stores')]</value>
      <webElementGuid>c036b74b-abad-49c8-95af-64a0dfdc4a62</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Secure payment'])[1]/following::a[1]</value>
      <webElementGuid>16edcfb4-8094-47c4-bd47-0d6768946fd0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='About us'])[1]/following::a[2]</value>
      <webElementGuid>dedf607e-8a74-44d8-a364-1b65ca47f146</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Specials'])[1]/preceding::a[1]</value>
      <webElementGuid>8df4a775-14a4-477e-b2dd-df4f1d8a0587</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Printed Summer Dress'])[1]/preceding::a[3]</value>
      <webElementGuid>7a1b9705-482e-4796-b728-00459048f9f0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Our stores']/parent::*</value>
      <webElementGuid>523111f4-0560-4a2b-9011-1886f8a937ed</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[@href='http://automationpractice.com/index.php?controller=stores']</value>
      <webElementGuid>d238a9bf-2b52-4c86-9aed-9f88fdee37b7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[6]/a</value>
      <webElementGuid>b4944ed4-98a0-461a-9d79-1418d4be56b5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'http://automationpractice.com/index.php?controller=stores' and @title = 'Our stores' and (text() = '
								Our stores
							' or . = '
								Our stores
							')]</value>
      <webElementGuid>e9581654-4053-43b0-9d82-884d7028c7a6</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
