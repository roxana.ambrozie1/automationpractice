<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>hlkLegal Notice</name>
   <tag></tag>
   <elementGuidId>9504a6ab-f0f8-4dad-89b7-da909939a592</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@title=&quot;Legal Notice&quot;]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a[title=&quot;Legal Notice&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>4d93d4f2-ac25-4edf-baa3-c839ec44dbff</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>http://automationpractice.com/index.php?id_cms=2&amp;controller=cms</value>
      <webElementGuid>32cb5671-5589-4cd4-a8d7-263a1f9f385d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Legal Notice</value>
      <webElementGuid>f1118951-180c-4a7f-8d44-2b5d4c423ca6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
									Legal Notice
								</value>
      <webElementGuid>3d7a0008-ede2-464e-8392-0a2bdf5f9efe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;informations_block_left_1&quot;)/div[@class=&quot;block_content list-block&quot;]/ul[1]/li[2]/a[1]</value>
      <webElementGuid>be870295-4baf-4c1b-a752-f639d0bf0687</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='informations_block_left_1']/div/ul/li[2]/a</value>
      <webElementGuid>3443914e-48e4-4953-a9ea-85d2c147b36e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Legal Notice')]</value>
      <webElementGuid>85fbbc40-aa30-4c26-8ad4-61cc0868b3d6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Delivery'])[1]/following::a[1]</value>
      <webElementGuid>67c193ed-ec4e-4650-a48a-722fe5fada3b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Information'])[1]/following::a[2]</value>
      <webElementGuid>a28377fc-73c7-4910-88c9-bec0d70fb45f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Terms and conditions of use'])[1]/preceding::a[1]</value>
      <webElementGuid>5f7addd4-9131-45e8-ae35-ebc28acd5db2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='About us'])[1]/preceding::a[2]</value>
      <webElementGuid>15376ddd-f5d2-47e3-a1e3-cfe12335d4d1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Legal Notice']/parent::*</value>
      <webElementGuid>dee09d57-daf9-44c4-9697-3ac66d00a8f5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[@href='http://automationpractice.com/index.php?id_cms=2&amp;controller=cms']</value>
      <webElementGuid>62baa1dd-4348-4c4a-8bee-e1c469408f4f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//section/div/ul/li[2]/a</value>
      <webElementGuid>eceed6b6-d38c-4918-b72d-84a5eaf8c507</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'http://automationpractice.com/index.php?id_cms=2&amp;controller=cms' and @title = 'Legal Notice' and (text() = '
									Legal Notice
								' or . = '
									Legal Notice
								')]</value>
      <webElementGuid>3bd21009-57c3-4ccd-a5fe-56f3ee4ac1ed</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
