<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>hlkSecurePayment</name>
   <tag></tag>
   <elementGuidId>aaab211a-8281-4c0b-833b-b28a702afe57</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@title=&quot;Secure payment&quot;]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a[title=&quot;Secure payment&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>3f316554-dcb3-44d4-baef-b589dbdd3f90</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>http://automationpractice.com/index.php?id_cms=5&amp;controller=cms</value>
      <webElementGuid>16e73483-18be-4709-8a24-e3b8a3683579</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Secure payment</value>
      <webElementGuid>1ef48d30-a4c0-4f0a-a9cf-e69b2aa55579</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
									Secure payment
								</value>
      <webElementGuid>bdf9d792-09df-4309-ab00-170d16e22fd8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;informations_block_left_1&quot;)/div[@class=&quot;block_content list-block&quot;]/ul[1]/li[5]/a[1]</value>
      <webElementGuid>8a2ec7aa-d75b-4033-a7e0-0b5b984aa631</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='informations_block_left_1']/div/ul/li[5]/a</value>
      <webElementGuid>8881fb64-25ac-4779-8e06-17e3b2074455</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Secure payment')]</value>
      <webElementGuid>fa521d1b-8119-4ed8-9c55-96ef9c2cee41</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='About us'])[1]/following::a[1]</value>
      <webElementGuid>b73eeebd-6dfd-4834-854f-14a9a675f44c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Terms and conditions of use'])[1]/following::a[2]</value>
      <webElementGuid>5e299e6a-b517-4480-af63-457c316f3a05</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Our stores'])[1]/preceding::a[1]</value>
      <webElementGuid>c3988ca3-a82b-4c84-b672-7e8f3957390d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Specials'])[1]/preceding::a[2]</value>
      <webElementGuid>fbd10f97-4a72-4580-a38e-c99d50d269df</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Secure payment']/parent::*</value>
      <webElementGuid>62aed60a-0365-4a7c-b554-c064cb1b8706</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[@href='http://automationpractice.com/index.php?id_cms=5&amp;controller=cms']</value>
      <webElementGuid>8f98d082-831a-4f66-94f2-6361df676e81</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[5]/a</value>
      <webElementGuid>92e6b7c7-8618-45e8-8ec7-bed6f410b229</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'http://automationpractice.com/index.php?id_cms=5&amp;controller=cms' and @title = 'Secure payment' and (text() = '
									Secure payment
								' or . = '
									Secure payment
								')]</value>
      <webElementGuid>beb1f99f-dda4-4efb-b6e5-a29502af3d5b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
