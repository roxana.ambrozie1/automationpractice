<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>hlkOurStores</name>
   <tag></tag>
   <elementGuidId>f48e2fbd-c984-46f0-a105-7366c3b5a0bc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//p[@class='title_block']/a[@title='Our stores']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>p.title_block > a[title=&quot;Our stores&quot;]
</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>983f6c04-89ed-4d88-88a3-bbe69ae58fdc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>http://automationpractice.com/index.php?controller=stores</value>
      <webElementGuid>f13a1b29-6d9f-470a-af94-d09d40e8b2a6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Our stores</value>
      <webElementGuid>6010e503-52b8-4c73-b165-c67a18b10f31</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
			Our stores
		</value>
      <webElementGuid>844e5682-0589-4d1a-b79f-ded81e027007</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;stores_block_left&quot;)/p[@class=&quot;title_block&quot;]/a[1]</value>
      <webElementGuid>5985bf12-a508-43d2-b2da-9529c01db2bb</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='stores_block_left']/p/a</value>
      <webElementGuid>ccd2c3c7-f5cb-42df-9847-8a6d8f4badce</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>(//a[contains(text(),'Our stores')])[2]</value>
      <webElementGuid>90a54dc1-2825-4c89-b32f-796d9df24d3d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='All specials'])[1]/following::a[1]</value>
      <webElementGuid>1b713554-6863-4fab-900f-cb0b086bd73d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='$30.51'])[1]/following::a[2]</value>
      <webElementGuid>6df61982-65fa-4746-b01a-ce280cd43d7a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Discover our stores'])[1]/preceding::a[2]</value>
      <webElementGuid>b441f890-b796-4175-b90f-1d7f861f1e1c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Women'])[4]/preceding::a[3]</value>
      <webElementGuid>aedf7c2c-fcf7-4aa4-97c9-e7b12c89d630</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[@href='http://automationpractice.com/index.php?controller=stores'])[2]</value>
      <webElementGuid>650cb038-5248-4c23-b14f-04c94dde747d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/p/a</value>
      <webElementGuid>d6322de9-af0a-461f-beb4-9ea6eab91bfa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'http://automationpractice.com/index.php?controller=stores' and @title = 'Our stores' and (text() = '
			Our stores
		' or . = '
			Our stores
		')]</value>
      <webElementGuid>24fcb868-0c2d-490e-bf97-933e63c9b9ed</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
