<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btnDiscoverOurStores</name>
   <tag></tag>
   <elementGuidId>cf494335-a614-49db-b9b5-fe07033d21ff</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>.button-small[title='Our stores']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[@class='btn btn-default button button-small'][@title='Our stores']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>e53adb59-65b4-4348-be1e-ac1cc23c5c62</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Discover our stores</value>
      <webElementGuid>4bca22cd-576a-445e-80e5-7d5442e86943</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;stores_block_left&quot;)/div[@class=&quot;block_content blockstore&quot;]/div[1]/a[@class=&quot;btn btn-default button button-small&quot;]/span[1]</value>
      <webElementGuid>2822a540-0613-41ea-abfa-ae41f0d1a06f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='stores_block_left']/div/div/a/span</value>
      <webElementGuid>d645f950-786e-4afd-a786-694257b47990</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Our stores'])[2]/following::span[1]</value>
      <webElementGuid>8a91b4b8-5658-469b-9436-152966462106</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='All specials'])[1]/following::span[1]</value>
      <webElementGuid>7c496523-8876-4056-8d8d-9c253c48f03c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Women'])[4]/preceding::span[1]</value>
      <webElementGuid>bf0201ac-7048-4359-b8e1-5c5760b3097e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='You will find here all woman fashion collections.'])[1]/preceding::span[2]</value>
      <webElementGuid>b4f6b076-ac82-4f1f-9a38-dd41afce7fbf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Discover our stores']/parent::*</value>
      <webElementGuid>d379057d-de72-443a-8f0f-7463fe43bae3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div/a/span</value>
      <webElementGuid>54722932-6f30-4aa8-b471-b916a4093c93</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Discover our stores' or . = 'Discover our stores')]</value>
      <webElementGuid>71aa85ba-9871-4c94-bfc7-44bbcc508c76</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
